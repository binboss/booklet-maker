import os
import sys
import asyncio
from pathlib import Path
from typing import Dict, List, Tuple, AsyncGenerator
import math
from PIL import Image as ImagePIL, ImageOps
from paper import get_paper_class, PaperType

if __name__ == '__main__':
    sys.path.append(os.path.abspath(os.path.join(
        os.path.dirname(__file__), os.path.pardir)))
sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(__file__), os.path.pardir)))

import project_config
from image import Image
from page import Page, Pages
from applib.policy_async import policy_async
from applib.log import logger
info, warn, error = logger.info, logger.warning, logger.error


class BasePanel(object):
    """..."""
    grids_on_unit_book_page = 2 # two grids as one large page

    def __init__(self, pages: Pages, grids: int):
        self.config = project_config.config
        self._pages: Pages = pages
        self._total_original_pages = len(self._pages.pages)
        self._preset: str = None
        self._resize_args: Dict = None
        self._resize_preset_size: Tuple[int, int] = None
        self.artifact_preset_paper_type = self.config.target_paper_type
        self._artifact_total_pages: int = 0
        self._artifact_preset_paper: PaperType = None
        self._artifact_bbox: Tuple[int, int, int, int] = None
        self._artifact_total_columns: int = 0
        self._artifact_total_rows: int = 0
        self._artifact_total_columns_rows: Tuple[int, int] = (0, 0)
        self._artifact_size: Tuple[int, int] = (0, 0)
        self._artifact_ratio: float = 0
        self._printer_border: int = None
        self._border: int = None
        self.grids: int = grids
        # self.new_book_dir: Path = self.new_book_src_dir / 'two'
        self.new_book_dir: Path = None
        self.curr_pagination_page: int = 0

        self._blank_image: str = None
        self._pages_ids: List[int] = None

        self.semaphore = asyncio.Semaphore(self.config.max_workers)

    def init(self) -> None:
        """..."""
        self.create_new_book_dir()

    @property
    def pages(self) -> Pages:
        """..."""
        return self._pages

    @pages.setter
    def pages(self, pages: Pages) -> None:
        """..."""
        self._pages = pages

    @property
    def preset(self) -> str:
        """..."""
        if self._preset is not None:
            return self._preset

        if self.grids == 2:
            self._preset = 'A5'
        elif self.grids == 4:
            self. _preset = 'A6'
        elif self.grids == 8:
            self._preset = 'A7'
        elif self.grids == 16:
            self._preset = 'A8'
        else:
            raise ValueError(f'No preset for grids {self.grids}')

        return self._preset

    @property
    def preset_paper_cls(self) -> PaperType:
        """..."""
        return get_paper_class(self.preset)

    @property
    def resize_preset_size(self) -> Tuple[int, int]:
        """..."""
        if self._resize_preset_size is not None:
            return self._resize_preset_size

        # self._resize_preset_size = self.artifact_paper.to_preset_size(self.pages.pages[0].image.image.size, dpi=self.config.dpi, landscape=self.config.landscape)
        # self._resize_preset_size = self.preset_paper_cls.preset_size_by_dpi(self.config.dpi)
        self._resize_preset_size = self.artifact_preset_paper.scale_size_to_preset(self.preset)
        return self._resize_preset_size

    @property
    def resize_args(self) -> Dict:
        """..."""
        if self._resize_args is not None:
            return self._resize_args

        # we need to convert the original border and margin to the new one that based on artifact size.
        # The border and the margin will be larger when the artifact is larger.
        border = self.printer_border + self.border
        margin = tuple([self.artifact_preset_paper.scale_px_by_dpi(self.artifact_size[0], margin, dpi=self.config.dpi) for margin in self.config.margin])
        caption_margin = tuple([self.artifact_preset_paper.scale_px_by_dpi(self.artifact_size[0], margin, dpi=self.config.dpi) for margin in self.config.pagination_margin])

        self._resize_args = dict(
            preset=self.preset,
            dpi=self.config.dpi,
            add_pagination=not self.config.no_pagination,
            pagination_prefix=self.config.pagination_prefix,
            pagination_suffix=self.config.pagination_suffix,
            pagination_starts_from=self.config.pagination_starts_from,
            border=border,
            margin=tuple(margin),
            caption_font=self.config.pagination_font,
            caption_font_size=self.config.pagination_font_size,
            caption_margin=caption_margin,
            paper_size=self.artifact_size,
            landscape=self.config.landscape,
            align='center' if not self.config.bookbinding_widthless else 'oddleftevenright'
        )

        return self._resize_args

    @property
    def total_papers(self) -> int:
        """..."""
        return math.ceil(self.total_pages / (self.grids * 2))

    @property
    def total_grids(self) -> int:
        return self.total_papers * self.grids * 2

    @property
    def pages_per_paper(self) -> int:
        """..."""
        return self.grids * 2

    @property
    def total_units_per_page(self) -> int:
        """..."""
        return self.grids / self.grids_on_unit_book_page

    @property
    def total_pages(self) -> int:
        """Total pages in the book or current part.
        You may need to use self.artifact_total_pages if you want to get total pages in the artifact.
        """
        return len(self.pages)

    @property
    def total_original_pages(self) -> int:
        """..."""
        return self._total_original_pages

    @property
    def artifact_total_pages(self) -> int:
        """..."""
        if self._artifact_total_pages == 0:
            return self.get_artifact_total_pages(self.total_original_pages)

        return self._artifact_total_pages

    @property
    def artifact_preset_paper(self) -> PaperType:
        """Artifact is a preset paper, e.g: A4.
        """
        if self._artifact_preset_paper is not None:
            return self._artifact_preset_paper

        self._artifact_preset_paper = get_paper_class(self.artifact_preset_paper_type).from_dpi(self.config.dpi)
        return self._artifact_preset_paper

    @property
    def artifact_size(self) -> Tuple[int, int]:
        """The size of the artifact"""
        if self._artifact_size == (0, 0):
            self._artifact_size = self._get_artifact_size()

        return self._artifact_size

    @property
    def artifact_ratio(self) -> float:
        """..."""
        if self._artifact_ratio == 0:
            self._artifact_ratio = self.artifact_size[0] / self.artifact_size[1]

        return self._artifact_ratio

    @property
    def artifact_total_columns(self) -> int:
        """..."""
        if self._artifact_total_columns == 0:
            self._artifact_total_columns = self.artifact_total_columns_rows[0]

        return self._artifact_total_columns

    @property
    def artifact_total_rows(self) -> int:
        """..."""
        if self._artifact_total_rows == 0:
            self._artifact_total_rows = self.artifact_total_columns_rows[1]

        return self._artifact_total_rows

    @property
    def artifact_total_columns_rows(self) -> Tuple[int, int]:
        """..."""
        if self._artifact_total_columns_rows == (0, 0):
            self._artifact_total_columns_rows = self.get_artifact_total_columns_rows()

        return self._artifact_total_columns_rows

    @property
    def artifact_is_landscape(self) -> bool:
        """..."""
        return self.artifact_size[0] > self.artifact_size[1]

    @property
    def blank_image(self) -> str:
        if not self._blank_image:
            self._blank_image = str((self.config.new_book_src_dir / 'blank.png').resolve())

        return self._blank_image

    @property
    def output_pdf_path(self) -> Path:
        """..."""
        return self.new_book_dir / '_book.pdf'

    @property
    def pages_ids(self) -> List[int]:
        """..."""
        if not self._pages_ids:
            self._pages_ids = [page.id for page in self.pages.pages]

        return self._pages_ids

    @property
    def printer_border(self) -> int:
        """..."""
        if self._printer_border is None:
            self._printer_border = self.artifact_preset_paper.scale_px_by_dpi(
                self.artifact_size[0] if not self.artifact_is_landscape else self.artifact_size[1],
                self.config.printer_border,
                dpi=self.config.dpi)

        return self._printer_border

    @property
    def border(self) -> int:
        """..."""
        if self._border is None:
            self._border = self.artifact_preset_paper.scale_px_by_dpi(
                self.artifact_size[0] if not self.artifact_is_landscape else self.artifact_size[1],
                self.config.border,
                dpi=self.config.dpi)

        return self._border

    def get_artifact_total_columns_rows(self) -> Tuple[int, int]:
        """Get the total columns and rows of the artifact.
        The total columns and rows is the number of unit book pages in the artifact.
        The artifact is the portrait preset page (Default A4) size of the book.
        The unit book page is a landscape unit on the artifact.

        Returns:
            Tuple[int, int]: The total columns and rows of the artifact.
        """
        width, height = self.resize_preset_size[0] * 2, self.resize_preset_size[1]
        # if artifact_ratio > 1:
        #     # artifact is landscape
        #     width, height = height, width

        indexrow = 0
        indexcolum = 0
        left = 0
        top = 0
        right = width
        bottom = 0
        while(right<=self.artifact_size[0]):
            bottom = height
            top = 0
            indexrow=0

            while(top<self.artifact_size[1]):
                top = bottom
                indexrow += 1
                bottom += height

            indexcolum+=1
            left = right
            right += width

        return indexcolum, indexrow

    def _get_artifact_size(self) -> Tuple[int, int]:
        """Get the size of the artifact by merging all the pages.

        Returns:
            Tuple[int, int]: The specified preset page (self.config.target_paper_type, e.g: A4) size of the artifact.
        """
        # Chose the first page as the reference, and get its A4 size
        # preset_page_size = self.artifact_preset_paper.to_preset_size(self.pages.pages[0].image.image.size, dpi=self.config.dpi, landscape=self.config.landscape)
        width, height = self.resize_preset_size[0], self.resize_preset_size[1]
        total_unit_pages = self.grids // 2
        # every 2 unit pages belong to one page.
        # new page size will be height, width * 2
        landscape = False
        while total_unit_pages > 0:
            _height = width * 2
            width = height
            height = _height
            landscape = not landscape
            total_unit_pages = total_unit_pages // 2

        if landscape:
            width, height = height, width

        return width, height

    def get_artifact_total_pages(self, pages: int) -> int:
        """..."""
        count = (self.pages_per_paper - pages % self.pages_per_paper) % self.pages_per_paper
        return pages + count

    def get_resize_args(self, part_id: int) -> Dict:
        """..."""
        resize_args = self.resize_args
        resize_args['pagination_starts_from'] = self.get_parts_pagination_starts_from(part_id)
        resize_args['custom_preset_size'] = self.resize_preset_size
        return resize_args

    def get_parts_pagination_starts_from(self, part_id: int = 1) -> int:
        """..."""
        if part_id == 1 or not self.config.parts_no_reset_pagination:
            return self.config.pagination_starts_from

        # Don't use self.total_pages, because it is updated by current part
        # And we need to get the pagination starts from of the previous part
        # every part should have a efective artifact pages, so we can use self.artifact_total_pages.
        starts_from = self.get_parts_pagination_starts_from(part_id - 1) + self.get_artifact_total_pages(self.artifact_total_pages // self.config.parts)
        if self.config.parts_reuse_first:
            starts_from -= 1

        return starts_from

    def create_new_book_dir(self) -> None:
        if not self.new_book_dir.exists():
            self.new_book_dir.mkdir(parents=True)

    async def create_blank_image(self) -> None:
        """..."""
        await self.create_preset_blank_image_async()

    async def create_preset_blank_image_async(self) -> None:
        """..."""
        await policy_async(self.create_preset_blank_image)

    def create_preset_blank_image(self) -> None:
        if not Path(self.blank_image).exists():
            blank_image = Image.blank_image(*(self.preset_paper_cls.preset_size))
            blank_image.save(self.blank_image)

    async def resize_pages(self, part_id: int = 1):
        """..."""
        info('Resizing pages, keep the pages in the same size.')
        resize_args = self.get_resize_args(part_id)
        self.pages = await self.pages.resize(
            expand=True,
            align=self.resize_args['align'],
            max_size=resize_args['custom_preset_size'],
            size_ignore_pages=(0,-1))

        info('Resizing pages, keep the pages in the same preset page size.')
        self.pages = await self.pages.resize_to_preset_page(
            **resize_args)

    def move_last_page_after_blank(self):
        info("Move last page after blank")
        pages: List[Page] = self.pages.pages
        if not pages[-1].is_blank:
            return
        i = -1
        while pages[i].is_blank:
            i -= 1
        pages = pages[:i] + pages[i + 1:] + [pages[i]]
        self.pages.pages = pages

    def add_blank(self) -> None:
        """Add blank image to the end of the list.

        Args:
            src: List[str]
            item_per_paper: int, number of items per paper, ex. 4 for two panel

        """
        if self.config.first_always_blank:
            self.pages.pages.insert(0, Page(self.pages.pages[0].id - 1, self.blank_image, is_blank=True))

        if self.config.blank_after_first:
            self.pages.pages.insert(1, Page(self.pages.pages[1].id - 0.001, self.blank_image, is_blank=True))

        info("add blank pages")
        count = (self.pages_per_paper - self.total_pages % self.pages_per_paper) % self.pages_per_paper
        info("original number of images: " + str(self.total_pages) +
                    ", number of blank images to add: " + str(count))

        # self.pages.pages.extend([Page('blank', self.blank_image, is_blank=True)] * count)
        for i in range(count):
            self.pages.pages.append(Page(self.pages.pages[-1].id + i + 1, self.blank_image, is_blank=True))

        if count == 0 and self.config.last_always_blank:
            self.pages.pages.append(Page(self.pages.pages[-1].id + 1, self.blank_image, is_blank=True))
            self.add_blank()

    def split_pages_into_parts(self) -> List[List[Page]]:
        """..."""
        total_pages_per_part = self.artifact_total_pages // self.config.parts
        total_pages_per_part = self.get_artifact_total_pages(total_pages_per_part)

        parts = []
        for i in range(self.config.parts):
            pages = self.pages.pages[i * total_pages_per_part: (i + 1) * total_pages_per_part]
            if i > 0:
                if self.config.parts_reuse_first:
                    pages.insert(0, self.pages.pages[0])
                    pages[0].id -= 1
                # reusing last page while current part is not the last
                if self.config.parts_reuse_last and i < self.config.parts - 1:
                    pages.append(self.pages.pages[-1])
                    pages[-1].id += 1

            parts.append(pages)

        return parts

    def get_pairs(self) -> List[List[Page]]:
        """..."""
        raise NotImplementedError

    def get_saddle_cutter_pairs(self) -> List[List[Page]]:
        """..."""
        raise NotImplementedError

    async def merge_pages_to_book_page_async(self, *pages: Page, **kwargs) -> ImagePIL.Image:
        """Merge images to one page"""
        # await asyncio.sleep(0.5)
        return await policy_async(self.merge_pages_to_book_page_artifact, *pages, **kwargs)

    def set_bookbinding_width(self, page: Page, page_po: str, twist: bool = False) -> Page:
        """Set bookbinding width for page according to page type

        Args:
            image: ImagePIL.Image
            page_po: str, the position of the page which is on the left or right of the landscape minimal unit page.
            twist: bool, if the page is twisted.

        Returns:
            ImagePIL.Image

        """
        # invert image (so that white is 0)
        im_inverted = ImageOps.invert(page.image.image)

        # Get bounding box of text and trim to it
        bbox = im_inverted.getbbox()


        if self.config.landscape:
            bookbinding_width = bbox[3]

        else:
            bookbinding_width = bbox[0] if page_po == 'right' else bbox[2]

        if bookbinding_width > self.config.bookbinding_width:
            # no need to expand image.
            return page

            # expand image
        image = ImageOps.expand(page.image.image, border=self.config.bookbinding_width - bookbinding_width, fill=255)
        page.image.image = image

        return page

    @property
    def artifact_bbox(self) -> Tuple[int, int, int, int]:
        """Get bounding box of artifact

        Args:
            im: ImagePIL.Image, the artifact image or the unit image of the artifact image.

        Returns:
            Tuple[int, int, int, int]
        """
        if self._artifact_bbox is not None:
            return self._artifact_bbox

        width, height = self.artifact_size
        border: int = self.resize_args.get('border')

        expanded_border = self.get_expanded_border(border, self.artifact_ratio)
        bbox = (border, expanded_border, width - border, height - expanded_border)

        self._artifact_bbox = tuple(bbox)
        return self._artifact_bbox

    def remove_border(self, im: ImagePIL.Image, keep_ratio = True) -> ImagePIL.Image:
        """Get bounding box of text and trim to it"""
        if self.config.printer_borderless:
            return im

        # Remove the border of the image. As the border will be added while printing, we need to remove it.
        # # in two panel mode with landscape, the border should not be removed.
        if self.config.landscape and self.grids == 2:
            return im

        # return im

        try:
            # return Image.auto_remove_border(im)
            if keep_ratio:
                bbox = self.artifact_bbox
            else:
                border = self.resize_args.get('border')
                bbox = (border, border, im.size[0] - border, im.size[1] - border)

            image = im.crop(bbox)

            return image

        finally:
            im.close()

    @staticmethod
    def get_expanded_border(border: int, ratio: float) -> int:
        """Get expanded border"""
        if ratio < 1:
            ratio = 1 / ratio

        return int(border * ratio)

    def remove_artifact_printer_border(self, im: ImagePIL.Image) -> ImagePIL.Image:
        """Remove printer border from image"""

        if self.config.printer_borderless:
            return im

        try:
            im = im.copy()
            border = self.printer_border
            width, height = self.artifact_size
            expanded_border = self.get_expanded_border(border, self.artifact_ratio)
            bbox = (border, expanded_border, width - border, height - expanded_border)

            image = im.crop(bbox)

            return image

        finally:
            im.close()

    def split_artifact(self, im: ImagePIL.Image) -> List[Tuple[Tuple[int, int, int, int], ImagePIL.Image]]:
        """Split artifact image into parts of unit pages

        Args:
            im: ImagePIL.Image, the artifact image or the unit image of the artifact image.

        Returns:
            List[Tuple[Tuple[int, int, int, int], ImagePIL.Image]], the list of unit pages with box and image.
        """
        # the unit page is the minimal unit page of the artifact image.
        # The unit page size
        width, height = self.resize_preset_size[0] * 2, self.resize_preset_size[1]

        # split the image into unit book pages
        unit_book_pages: List[List[Tuple[int, int, int, int], ImagePIL.Image]] = []
        # indexrow = 0
        # indexcolum = 0
        left = 0
        top = 0
        right = width
        bottom = 0
        while(right<=self.artifact_size[0]):
            bottom = height
            top = 0
            # indexrow=0

            while(top<self.artifact_size[1]):
                # t = int(time.time() * 1000)
                cropimg= im.crop((left, top, right, bottom))
                unit_book_pages.append((
                    (left, top, right, bottom),
                    cropimg
                ))
                # Image.save(cropimg, self.new_book_dir / f'{t}_{indexrow}_{indexcolum}.jpg')
                top = bottom
                # indexrow += 1
                bottom += height

            # indexcolum+=1
            left = right
            right += width

        return unit_book_pages

    def merge_pages_to_book_page_artifact(self, *pages: Page, twist: bool = False) -> ImagePIL.Image:
        """Merge images to one page as artifact.
        """
        total_grids = len(pages)
        total_unit_book_pages = total_grids // self.grids_on_unit_book_page

        unit_book_pages: List[ImagePIL.Image] = []
        for index in range(total_unit_book_pages):
            unit_book_page_pages: List[Page] = pages[index * self.grids_on_unit_book_page: (index + 1) * self.grids_on_unit_book_page]
            # assert len(unit_book_page_pages) == self.grids_on_unit_book_page
            if self.config.landscape:
                unit_book_page_pages = [page.transpose(ImagePIL.ROTATE_90) for page in unit_book_page_pages]

            width = sum([page.image.image.width for page in unit_book_page_pages])
            height = max([page.image.image.height for page in unit_book_page_pages])
            unit_page_image = ImagePIL.new("RGB", (width, height))
            unit_page_image.paste((255, 255, 255), (0, 0, width, height))
            unit_page_image.paste(unit_book_page_pages[0].image.image, (0, 0))
            unit_book_page_pages[0].image.release()
            for i, page in enumerate(unit_book_page_pages[1:]):
                left_w = sum([page.image.image.width for page in unit_book_page_pages[:i + 1]])
                unit_page_image.paste(page.image.image, (left_w, 0))
                page.image.release()

            unit_book_pages.append(unit_page_image)

        # close all pages
        for page in pages:
            page.image.release()

        if total_unit_book_pages == 1:
            page_image = unit_book_pages[0]
            if twist:
                # rotate back to opposite direction
                page_image = page_image.transpose(ImagePIL.ROTATE_180)
            return self.remove_artifact_printer_border(page_image)

        # merge unit book pages to one page as a new larger unit page
        rotate_90_times = 0
        _total_unit_book_pages = total_unit_book_pages
        while _total_unit_book_pages // 2 != 0:
            _total_unit_book_pages //= 2
            rotate_90_times += 1

        # generate random string
        # s = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(8))
        rotated = False
        while len(unit_book_pages) > 1:
            begin = 0
            for i in range(0, total_unit_book_pages, 2):
                top_page = unit_book_pages[begin]
                bottom_page = unit_book_pages[begin + 1]
                # create empty page in landscape mode
                page_image = ImagePIL.new("RGB", (top_page.height * 2, top_page.width))
                # paste top page as left page
                method = ImagePIL.ROTATE_90 if not rotated else ImagePIL.ROTATE_270
                page_image.paste(top_page.transpose(method), (0, 0))
                # Image.save(page_image, self.new_book_dir / f'{s}_0.png')
                # paste bottom page as right page
                page_image.paste(bottom_page.transpose(method), (top_page.height, 0))
                # Image.save(page_image, self.new_book_dir / f'{s}_1.png')
                # rotate back to the original direction
                # not twist, and only two unit book pages left (left and right) which are the last two pages to merge to the final page
                if len(unit_book_pages) <= self.grids_on_unit_book_page:
                    rotate = 360 - rotate_90_times * 90 if not rotated else 0 # original direction
                    rotate = rotate + 180 if twist else rotate # opposite direction while twist
                    if rotate not in [0, 360]:
                        page_image = page_image.rotate(rotate, expand=True)

                top_page.close()
                bottom_page.close()

                # Image.save(page_image, self.new_book_dir / f"{s}_{2}.png")
                unit_book_pages[begin] = page_image
                unit_book_pages.pop(begin + 1)
                begin += 1
                total_unit_book_pages -= 1

            rotated = not rotated

        # return unit_book_pages[0]
        # return self.remove_border(unit_book_pages[0])
        return self.remove_artifact_printer_border(unit_book_pages[0])

    async def generate_new_book_pages_for_parts(self):
        if self.config.parts == 1:
            self.add_blank()
            if self.config.last_after_blank:
                self.move_last_page_after_blank()

            await self.generate_new_book_pages()
            return

        info(f'Generating new book pages for parts.')

        # First, should add the book page to the book page list.
        self.add_blank()
        if self.config.last_after_blank:
            self.move_last_page_after_blank()

        # split the pages into parts
        parts = self.split_pages_into_parts()
        new_book_dir = self.new_book_dir
        for index, part in enumerate(parts):
            part_num = index + 1
            self.new_book_dir = new_book_dir / f'part_{part_num}'
            self.create_new_book_dir()
            self.pages.pages = part
            self.add_blank()
            await self.generate_new_book_pages(part_num)

    async def generate_new_book_pages(self, part: int = 1):
        """Generate new book pages."""
        raise NotImplementedError

    async def pairs_to_pages(self, pairs: List[List[Page]], disable_twist: bool = False) -> AsyncGenerator[ImagePIL.Image, None]:
        """Generate pages from pairs of images.

        The page is generated in landscape mode.
        """
        twist = False
        for i, pair in enumerate(pairs):
            info("Merge pair " + str(i + 1) + " to page")
            # generate random name for the page
            yield await self.merge_pages_to_book_page_async(*pair, twist=twist)
            twist = not twist if not disable_twist else False


    def pages_to_pdf(self, pages: List[str]) -> None:
        """..."""
        img = ImagePIL.open(pages[0])
        Image.save(img, self.output_pdf_path, format="pdf")
        img.close()
        for img in map(ImagePIL.open, pages[1:]):
            Image.save(img, self.output_pdf_path, format="pdf", append=True)
            img.close()

    def pages_to_pdf_front_back(self, pages: List[str]) -> None:
        """..."""
        total_pages = len(pages)
        # odd pages are front pages, even pages are back pages
        output_pdf_path = self.output_pdf_path.parent / self.output_pdf_path.name.replace(".pdf", "_front_back.pdf")
        img = ImagePIL.open(pages[0])
        Image.save(img, output_pdf_path, format="pdf")
        img.close()
        for i in range(2, total_pages, 2):
            img = ImagePIL.open(pages[i])
            Image.save(img, output_pdf_path, format="pdf", append=True)
            img.close()
        for i in range(1, total_pages, 2):
            img = ImagePIL.open(pages[i])
            Image.save(img, output_pdf_path, format="pdf", append=True)
            img.close()