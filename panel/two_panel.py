import os
import sys
from pathlib import Path
from typing import List, Tuple

if __name__ == '__main__':
    sys.path.append(os.path.abspath(os.path.join(
        os.path.dirname(__file__), os.path.pardir)))
sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(__file__), os.path.pardir)))

from page import Page
from image import Image
from panel.base_panel import BasePanel
from applib.log import logger
info, warn, debug, error = logger.info, logger.warning, logger.debug, logger.error


class TwoPanel(BasePanel):

    def __init__(self, *args, **kwargs):
        """..."""
        super().__init__(*args, grids=2, **kwargs)
        self.new_book_dir: Path = self.config.new_book_src_dir / 'two'
        self.init()

    async def generate_new_book_pages(self, part: int = 1):
        info(f'Generating new book pages.')

        # generate a blank image
        # self.blank_image = html_to_image.to_image('about:blank',
        #                                           f'{self.new_book_src_dir}/blank.png', need_crop=False, viewport={'width': 595, 'height': 842})[0]
        # self.blank_image = (await html_to_image.to_image('about:blank',
        #                                           f'{self.new_book_src_dir}/blank.png', need_crop=False, auto_close=False))[0]
        await self.create_blank_image()
        await self.resize_pages(part)

        if self.config.saddle or self.config.cutter:
            pairs = self.get_saddle_cutter_pairs()

        else:
            pairs = self.get_pairs()


        info(f'Generating book pages.')
        pages: List[str] = []
        front_page_num = 0
        back_page_num = 0
        i = 0
        async for page in self.pairs_to_pages(pairs, disable_twist=not self.config.landscape):
            i += 1
            if i % 2 == 0:
                back_page_num += 1
                out = self.new_book_dir / f'back_{back_page_num}{self.config.output_page_extension}'
            else:
                front_page_num += 1
                out = self.new_book_dir / f'font_{front_page_num}{self.config.output_page_extension}'

            Image.save(page, out)
            page.close()
            pages.append(out)

        info(f'Generating book PDF.')
        self.pages_to_pdf(pages)
        self.pages_to_pdf_front_back(pages)

    def get_pairs(self) -> List[List[Page]]:
        """..."""
        pairs: List[List[str]] = []
        for i in range(0, self.total_pages, self.grids):
            pairs.append(self.pages.pages[i:i + self.grids])

        return pairs

    def get_saddle_cutter_pairs(self) -> List[Tuple[Page]]:
        """Rearrange the pages to the pairs of front and back pages"""
        info("Rearrange pages for saddle and cutter.")
        dst: List[Tuple[Page]] = []
        begin = 0
        end = self.total_pages - 1
        twist = False
        while begin < end:
            if self.config.landscape:
                pair = (self.pages.pages[end], self.pages.pages[begin])
            else:
                if not twist:
                    pair = (self.pages.pages[end], self.pages.pages[begin])
                else:
                    pair = (self.pages.pages[begin], self.pages.pages[end])
            dst.append(pair)
            twist = not twist
            begin += 1
            end -= 1

        return dst

