import os
import sys
from pathlib import Path
from typing import List, Tuple

if __name__ == '__main__':
    sys.path.append(os.path.abspath(os.path.join(
        os.path.dirname(__file__), os.path.pardir)))
sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(__file__), os.path.pardir)))

from page import Page
from image import Image
from panel.base_panel import BasePanel

from applib.log import logger
debug, info, warn, error = logger.debug, logger.info, logger.warning, logger.error

class EightPanel(BasePanel):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, grids=8, **kwargs)
        self.new_book_dir: Path = self.config.new_book_src_dir / 'eight'
        self.init()

    async def generate_new_book_pages(self, part: int = 1):
        logger.info(f'Generating new book pages.')

        await self.create_blank_image()
        await self.resize_pages(part)

        if self.config.saddle or self.config.cutter:
            pairs = self.get_saddle_cutter_pairs()

        else:
            pairs = self.get_pairs()


        info(f'Generating book pages.')
        pages: List[str] = []
        front_page_num = 0
        back_page_num = 0
        i = 0
        async for page in self.pairs_to_pages(pairs, disable_twist=not self.config.landscape):
            i += 1
            if i % 2 == 0:
                back_page_num += 1
                out = self.new_book_dir / f'back_{back_page_num}{self.config.output_page_extension}'
            else:
                front_page_num += 1
                out = self.new_book_dir / f'font_{front_page_num}{self.config.output_page_extension}'

            Image.save(page, out)
            page.close()
            pages.append(out)

        info(f'Generating book PDF.')
        self.pages_to_pdf(pages)
        self.pages_to_pdf_front_back(pages)

    def get_pairs(self) -> List[List[Page]]:
        """..."""
        pairs: List[List[str]] = []
        for i in range(0, self.total_pages, self.grids * 2):
            pairs.append(self.pages.pages[i:i + self.grids * 2])

        return pairs

    def get_saddle_cutter_pairs(self) -> List[Tuple[Page]]:
        """Rearrange the pages to the pairs of front and back pages"""
        # A4 horizontal Paper front grid areas
        '''4 papers, each with 8 pages (grids) on each side

page 1:

front:                    back:

|    |    |    |    |     |    |    |    |    |
| 64 | 01 | 48 | 17 |     | 63 | 02 | 47 | 18 |
|    |    |    |    |     |    |    |    |    |
---------------------     ---------------------
|    |    |    |    |     |    |    |    |    |
| 56 | 09 | 40 | 25 |     | 55 | 10 | 39 | 26 |
|    |    |    |    |     |    |    |    |    |

page 2:

front:                    back:

|    |    |    |    |     |    |    |    |    |
| 62 | 03 | 46 | 19 |     | 61 | 04 | 45 | 20 |
|    |    |    |    |     |    |    |    |    |
---------------------     ---------------------
|    |    |    |    |     |    |    |    |    |
| 54 | 11 | 38 | 27 |     | 53 | 12 | 37 | 28 |
|    |    |    |    |     |    |    |    |    |

page 3:

front:                    back:

|    |    |    |    |     |    |    |    |    |
| 60 | 05 | 44 | 21 |     | 59 | 06 | 43 | 22 |
|    |    |    |    |     |    |    |    |    |
---------------------     ---------------------
|    |    |    |    |     |    |    |    |    |
| 52 | 13 | 36 | 29 |     | 51 | 14 | 35 | 30 |
|    |    |    |    |     |    |    |    |    |

page 4:

front:                    back:

|    |    |    |    |     |    |    |    |    |
| 58 | 07 | 42 | 23 |     | 57 | 08 | 41 | 24 |
|    |    |    |    |     |    |    |    |    |
---------------------     ---------------------
|    |    |    |    |     |    |    |    |    |
| 50 | 15 | 34 | 31 |     | 49 | 16 | 33 | 32 |
|    |    |    |    |     |    |    |    |    |
'''


        info("Rearrange pages for saddle and cutter.")
        dst: List[Tuple[Page]] = []
        begin = 0
        end = self.total_pages - 1
        total_pairs = self.total_pages // self.grids
        twist = False
        while begin <= total_pairs - 1:
            if self.config.landscape:
                if not twist:
                    pair = (self.pages.pages[begin], self.pages.pages[begin + total_pairs],
                            self.pages.pages[end], self.pages.pages[end - total_pairs],
                            self.pages.pages[begin + total_pairs * 2], self.pages.pages[begin + total_pairs * 3],
                            self.pages.pages[end - total_pairs * 2], self.pages.pages[end - total_pairs * 3],
                            )
                else:
                    pair = (self.pages.pages[end], self.pages.pages[end - total_pairs],
                            self.pages.pages[begin], self.pages.pages[begin + total_pairs],
                            self.pages.pages[end - total_pairs * 2], self.pages.pages[end - total_pairs * 3],
                            self.pages.pages[begin + total_pairs * 2], self.pages.pages[begin + total_pairs * 3])

            else:
                if not twist:
                    pair = (self.pages.pages[end], self.pages.pages[begin],
                            self.pages.pages[end - total_pairs], self.pages.pages[begin + total_pairs],
                            self.pages.pages[end - total_pairs * 2], self.pages.pages[begin + total_pairs * 2],
                            self.pages.pages[end - total_pairs * 3], self.pages.pages[begin + total_pairs * 3])
                else:
                    pair = (self.pages.pages[begin + total_pairs * 2], self.pages.pages[end - total_pairs * 2],
                            self.pages.pages[begin + total_pairs * 3], self.pages.pages[end - total_pairs * 3],
                            self.pages.pages[begin], self.pages.pages[end],
                            self.pages.pages[begin + total_pairs], self.pages.pages[end - total_pairs])
                    # pair = (self.pages.pages[begin+5], self.pages.pages[begin+2], self.pages.pages[end], self.pages.pages[begin])
            dst.append(pair)
            twist = not twist
            begin += 1
            end -= 1

        return dst