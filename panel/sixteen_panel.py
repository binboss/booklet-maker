import os
import sys
from pathlib import Path
from typing import List, Tuple
import numpy as np

if __name__ == '__main__':
    sys.path.append(os.path.abspath(os.path.join(
        os.path.dirname(__file__), os.path.pardir)))
sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(__file__), os.path.pardir)))

from page import Page
from image import Image
from panel.base_panel import BasePanel

from applib.log import logger
debug, info, warn, error = logger.debug, logger.info, logger.warning, logger.error

class SixteenPanel(BasePanel):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, grids=16, **kwargs)
        self.new_book_dir: Path = self.config.new_book_src_dir / 'sixteen'
        self.init()

    async def generate_new_book_pages(self, part: int = 1):
        logger.info(f'Generating new book pages.')

        await self.create_blank_image()
        await self.resize_pages(part)

        if self.config.saddle or self.config.cutter:
            pairs = self.get_saddle_cutter_pairs()

        else:
            pairs = self.get_pairs()


        info(f'Generating book pages.')
        pages: List[str] = []
        front_page_num = 0
        back_page_num = 0
        i = 0
        async for page in self.pairs_to_pages(pairs, disable_twist=self.config.landscape):
            i += 1
            if i % 2 == 0:
                back_page_num += 1
                out = self.new_book_dir / f'back_{back_page_num}{self.config.output_page_extension}'
            else:
                front_page_num += 1
                out = self.new_book_dir / f'font_{front_page_num}{self.config.output_page_extension}'

            Image.save(page, out)
            page.close()
            pages.append(out)

        info(f'Generating book PDF.')
        self.pages_to_pdf(pages)
        self.pages_to_pdf_front_back(pages)

    def get_pairs(self) -> List[List[Page]]:
        """..."""
        pairs: List[List[str]] = []
        for i in range(0, self.total_pages, self.grids * 2):
            pairs.append(self.pages.pages[i:i + self.grids * 2])

        return pairs

    def get_saddle_cutter_pairs(self) -> List[Tuple[Page]]:
        """Rearrange the pages to the pairs of front and back pages"""
        # A4 horizontal Paper front grid areas
        '''2 papers, each with 16 pages (grids) on each side

page 1:

front:                    back:

|    |    |    |    |     |    |    |    |    |
| 64 | 01 | 56 | 09 |     | 63 | 02 | 55 | 10 |
|    |    |    |    |     |    |    |    |    |
---------------------     ---------------------
|    |    |    |    |     |    |    |    |    |
| 60 | 05 | 52 | 13 |     | 59 | 06 | 51 | 14 |
|    |    |    |    |     |    |    |    |    |
---------------------     ---------------------
|    |    |    |    |     |    |    |    |    |
| 48 | 17 | 40 | 25 |     | 47 | 18 | 39 | 26 |
|    |    |    |    |     |    |    |    |    |
---------------------     ---------------------
|    |    |    |    |     |    |    |    |    |
| 44 | 21 | 36 | 29 |     | 43 | 22 | 35 | 30 |
|    |    |    |    |     |    |    |    |    |

page 2:

front:                    back:

|    |    |    |    |     |    |    |    |    |
| 62 | 03 | 54 | 11 |     | 61 | 04 | 53 | 12 |
|    |    |    |    |     |    |    |    |    |
---------------------     ---------------------
|    |    |    |    |     |    |    |    |    |
| 58 | 07 | 50 | 15 |     | 57 | 08 | 49 | 16 |
|    |    |    |    |     |    |    |    |    |
---------------------     ---------------------
|    |    |    |    |     |    |    |    |    |
| 46 | 19 | 38 | 27 |     | 45 | 20 | 37 | 28 |
|    |    |    |    |     |    |    |    |    |
---------------------     ---------------------
|    |    |    |    |     |    |    |    |    |
| 42 | 23 | 34 | 31 |     | 41 | 24 | 33 | 32 |
|    |    |    |    |     |    |    |    |    |
'''
        info("Rearrange pages for saddle and cutter.")
        dst: List[Tuple[Page]] = []
        begin = 0
        end = self.total_pages - 1
        total_pairs = self.total_pages // self.grids
        twist = False
        while begin <= total_pairs - 1:
            unit_pairs_indexies = [[begin + total_pairs * i, end - total_pairs * i] for i in range(8)]
            # >>> np.asarray(unit_pairs_indexies).reshape(4, 2, 2)
            # array([[[ 2, 31],
            #         [ 4, 29]],

            #       [[ 6, 27],
            #         [ 8, 25]],

            #       [[10, 23],
            #         [12, 21]],

            #       [[14, 19],
            #         [16, 17]]])
            unit_pairs_indexies = np.asarray(unit_pairs_indexies).reshape(4, 2, 2)
            if self.config.landscape:
                # transpose the array
                # >>> unit_pairs_indexies.transpose((0, 2, 1))
                # >>> array([[[ 2,  4],
                #             [31, 29]],

                #            [[ 6,  8],
                #             [27, 25]],

                #            [[10, 12],
                #             [23, 21]],

                #            [[14, 16],
                #             [19, 17]]])
                unit_pairs_indexies = unit_pairs_indexies.transpose((0, 2, 1))
                if twist:
                    # flip the array
                    unit_pairs_indexies = unit_pairs_indexies.reshape(2,2,2,2)
                    unit_pairs_indexies = np.flip(unit_pairs_indexies,0)
                    unit_pairs_indexies = np.flip(unit_pairs_indexies,2)
                    pass

                pair = [self.pages.pages[i] for i in unit_pairs_indexies.reshape(-1)]

            else:
                # group pairs by 2
                # >>> np.asarray(unit_pairs_indexies).reshape(2,2,2,2)
                # >>> np.array([
                #     [
                #         [[2,31],[4,29]],
                #         [[6,27],[8,25]]
                #     ],
                #     [
                #         [[10,23],[12,21]],
                #         [[14,19],[16,17]]
                #     ]
                # ])
                unit_pairs_indexies = unit_pairs_indexies.reshape(2,2,2,2)
                if not twist:
                    # just flip the pairs in the last axis
                    # >>> np.flip(unit_pairs_indexies,3)
                    # >>> array([[[[31,  2],
                    #              [29,  4]],

                    #             [[27,  6],
                    #              [25,  8]]],


                    #            [[[23, 10],
                    #              [21, 12]],

                    #             [[19, 14],
                    #              [17, 16]]]])
                    unit_pairs_indexies = np.flip(unit_pairs_indexies,3)
                else:
                    # flip the pairs in the first axis
                    # >>> np.flip(unit_pairs_indexies,1)
                    # >>> array([[[[ 6, 27],
                    #              [ 8, 25]],

                    #             [[ 2, 31],
                    #              [ 4, 29]]],


                    #            [[[14, 19],
                    #              [16, 17]],

                    #             [[10, 23],
                    #              [12, 21]]]])
                    unit_pairs_indexies = np.flip(unit_pairs_indexies,1)
                    # while twist is enabled in the later stage `pairs_to_pages`, the pairs should not be flipped in the first axis
                    # unit_pairs_indexies = np.flip(unit_pairs_indexies)

                pair = [self.pages.pages[i] for i in unit_pairs_indexies.reshape(-1)]

            dst.append(pair)
            twist = not twist
            begin += 1
            end -= 1

        return dst