import os
import sys
import asyncio
from pathlib import Path
from pyppeteer import launch
from pdf2image import convert_from_path
from pdfCropMargins import crop
import pdfkit
from typing import List, Union
from natsort import os_sorted
from concurrent.futures.process import BrokenProcessPool

if __name__ == '__main__':
    sys.path.append(os.path.abspath(os.path.join(
        os.path.dirname(__file__), os.path.pardir)))
sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(__file__), os.path.pardir)))

from applib.interrupt import KeyboardInterruptBlocked
from applib.log import logger
from common.env import APP_DIR
from services.crop_service import CropService
# import applib.pdfCropMargins
from applib.policy_async import policy_async
from utils.helper import wait_file_ready, merge_dict


USER_DATA_DIR = APP_DIR / 'temp/.dev_profile'

BROWSER = {
    'instance': None,
    'options': {
        'headless': True,
        'devtools': False,
        # 'timeout': 1000 * 5,
        'dumpio': True,               # 解决浏览器多开卡死
        'autoClose': False,
        'ignoreDefaultArgs': ['--enable-automation'],
        'userDataDir': USER_DATA_DIR,
        'handleSIGINT': False,
        'handleSIGTERM': False,
        'handleSIGHUP': False,
        'ignoreHTTPSErrors': True,
        'args': [
            '--no-sandbox',  # --no-sandbox 在 docker 里使用时需要加入的参数，不然会报错
            '--disable-dev-shm-usage', #By default, Docker runs a container with a /dev/shm shared memory space 64MB. This is typically too small for Chrome and will cause Chrome to crash when rendering large pages. To fix, run the container with docker run --shm-size=1gb to increase the size of /dev/shm. Since Chrome 65, this is no longer necessary
            '--disable-gpu',
            '--disable-extensions',
            '--disable-infobars',
            '--ignore-certificate-errors',
            '--ignore-ssl-errors',
            '--hide-scrollbars',
            '--disable-bundled-ppapi-flash',
            '--mute-audio',
            '--disable-setuid-sandbox',
            '--disable-xss-auditor',
            # '--single-process', # <- this one doesn't works in Windows
            # '--proxy-server=127.0.0.1:8080'
        ],
        'defaultViewport': None
    }
}


BROWSER_BIN = os.environ.get('BROWSER_BIN')
if BROWSER_BIN:
    BROWSER['options']['executablePath'] = BROWSER_BIN

async def to_image(fpath: Union[str, Path], outpath: Union[str, Path], tries=3, options: dict = None, **kwargs) -> List[Path]:
    """
        Options:

        * ``need_crop`` (bool): Whether to crop. Defaults to ``False``.
        * ``viewport`` (dict)

          * ``width`` (int): page width in pixels.
          * ``height`` (int): page width in pixels.
        * ``landscape`` (bool): Specifies if viewport is in landscape mod. Defaults to ``False``.
        * ``auto_close`` (bool): Whether to auto close browser. Defaults to ``True`` .

    """

    options = merge_dict(options, kwargs)
    options['need_crop'] = options.get('need_crop', False)
    options['viewport'] = options.get('viewport', None)
    options['landscape'] = options.get('landscape', False)
    options['auto_close'] = options.get('auto_close', True)

    while tries > 0:
        try:
            with KeyboardInterruptBlocked():
                # url = f'file://{fpath}'
                # sync_exec(_to_image, url, outpath=outpath, viewport=viewport)
                return await _to_image(fpath, outpath, options=options)

        except BrokenProcessPool:
            raise

        except Exception:
            logger.exception('Failed to convert to image.')
            tries -= 1

            if tries == 0:
                raise

            return await to_image(fpath, outpath, tries=tries, options=options)

    return []


async def _corp_pdf(pdfpath: Path) -> Path:
    min_pdf_path = pdfpath.parent / f'{pdfpath.stem}_min.pdf'
    await policy_async(crop, ["-p", "0", "-t", "255", os.fspath(pdfpath), '-o', os.fspath(min_pdf_path)], loop=asyncio.get_event_loop())
    # pdfpath.unlink()
    # min_pdf_path.rename(pdfpath)


async def _to_pdf(fpath: Union[str, Path], outpath: Union[str, Path], options: dict = None) -> Path:
    pdfpath = await _to_pdf_headless(fpath=fpath,
                                    outpath=outpath, options=options)
    return pdfpath


async def _to_pdf_headless(fpath: Union[str, Path], outpath: Union[str, Path], options: dict = None) -> Path:
    if Path(fpath).is_file():
        url = f'file://{os.fspath(Path(fpath))}'

    else:
        url = fpath

    browser_options = merge_dict(BROWSER['options'], options.pop('browser_options', {}))

    if browser_options != BROWSER['options'] or BROWSER['instance'] is None or (BROWSER['instance'].process.returncode and BROWSER['instance'].process.returncode < 0):
        browser = await launch(options=browser_options)
        BROWSER['instance'] = browser

    else:
        browser = BROWSER['instance']

    pdfpath = outpath.parent / f'{outpath.stem}.pdf'
    page = await browser.newPage()
    await page.goto(url, timeout=10000)
    await page.emulateMedia('screen')

    dimensions = await page.evaluate('''() => {
        return {
            width: Math.max(
                document.body.scrollWidth,
                document.documentElement.scrollWidth,
                document.body.offsetWidth,
                document.documentElement.offsetWidth,
                document.documentElement.clientWidth
            ),
            height: Math.max(
                document.body.scrollHeight,
                document.documentElement.scrollHeight,
                document.body.offsetHeight,
                document.documentElement.offsetHeight,
                document.documentElement.clientHeight
            ),
            deviceScaleFactor: window.devicePixelRatio,
        }
    }''')

    # no idea while the dpi is 144, but not the 96 or even other else.
    print_pdf_dpi = 72 * 2

    if options['landscape']:
        px_w = print_pdf_dpi * 11.7
        px_h = print_pdf_dpi * 8.27
    else:
        px_w = print_pdf_dpi * 8.27
        px_h = print_pdf_dpi * 11.7

    scroll = 'x' if px_w / px_h < (dimensions['width'] / dimensions['height']) else 'y'

    if scroll == 'x':
        scale = px_w / dimensions['width']
    else:
        scale = px_h / dimensions['height']

    # do need to scale
    if scale < 1:
        scale = int(scale * 100)
        scale = scale / 100

    else:
        scale = 1

    await page.pdf({
        'path': pdfpath,
        'format': 'A4',
        'pageRanges': '1',
        'landscape': options['landscape'],
        'printBackground': True,
        'scale': scale
    })

    await page.close()

    if options['auto_close']:
        await browser.close()
        BROWSER['instance'] = None

    # crop pdf margins
    # While crop is enabled, the pdf that need to be cropped will be put to this queue.
    if options['need_crop']:
        # await _corp_pdf(pdfpath)
        await CropService.crop_queue.put(pdfpath)
        await wait_for_min_pdf(pdfpath)

    return pdfpath


def _to_pdf_wkhtmltopdf(fpath: Union[str, Path], outpath: Union[str, Path], need_crop=False) -> Path:
    if Path(fpath).is_file():
        fpath_abs = os.fspath(Path(fpath))
        pdfkit_action = pdfkit.from_file
    else:
        fpath_abs = fpath
        pdfkit_action = pdfkit.from_url

    # html to pdf first, then convert to image
    pdfpath = outpath.parent / f'{outpath.stem}.pdf'
    pdfpath_abs = os.fspath(pdfpath)
    pdfkit_action(fpath_abs, pdfpath_abs, options={
        'page-size': 'A4',
        'margin-top': '0',
        'margin-right': '0',
        'margin-bottom': '0',
        'margin-left': '0',
        'no-outline': None
    })

    # crop pdf margins
    if need_crop:
        _corp_pdf(pdfpath)

    return pdfpath


async def _to_image(fpath: Union[str, Path], outpath: Union[str, Path], options: dict = None) -> List:
    loop = asyncio.get_event_loop()

    if Path(fpath).is_file():
        fpath_abs = os.fspath(Path(fpath))
        pdfkit_action = pdfkit.from_file
    else:
        fpath_abs = fpath
        pdfkit_action = pdfkit.from_url

    outpath = Path(outpath)

    pdfpath = await _to_pdf(fpath, outpath, options=options)

    # pdf to image
    pdf_image_path = outpath.parent / f'pdf_image_{pdfpath.stem}'
    pdf_image_path.mkdir(exist_ok=True)
    await policy_async(convert_from_path, pdfpath, output_folder=pdf_image_path,
                      output_file=outpath.stem, fmt='png', loop=loop)
    pdfpath.unlink()

    images = []
    for image in os_sorted(pdf_image_path.glob("*.png")):
        image_path = Path(image)
        parts = list(image_path.parts)
        parts.pop(-2)
        new_image_path = Path(*parts)
        if new_image_path.exists():
            new_image_path.unlink()

        await wait_file_ready(image_path)
        image_path.rename(new_image_path)
        images.append(new_image_path)

    pdf_image_path.rmdir()
    return images


async def _to_image_headless(url: str, outpath: str, viewport=None, tries=3):
    if viewport is not None:
        browser = await launch(
            headless=True,
            handleSIGINT=True,
            handleSIGTERM=True,
            handleSIGHUP=True,
            defaultViewport=viewport)
    else:
        browser = await launch(
            headless=True,
            handleSIGINT=True,
            handleSIGTERM=True,
            handleSIGHUP=True
        )
    page = await browser.newPage()
    await page.goto(url, timeout=10000)
    await page.emulateMedia('screen')
    await page.pdf({
        'path': Path(outpath).parent / f'{Path(outpath).stem}.pdf',
        'format': 'A4'})
    # element = await page.J('body')
    crop(["-p", "0", "-u", "-s", os.fspath(Path(outpath).parent /
                                           f'{Path(outpath).stem}.pdf'), '-o', os.fspath(Path(outpath).parent / f'{Path(outpath).stem}.pdf')])
    convert_from_path(Path(outpath).parent /
                      f'{Path(outpath).stem}.pdf', output_folder=Path(outpath).parent, fmt='png')
    # await element.screenshot({'path': outpath, 'omitBackground': True})
    await browser.close()
