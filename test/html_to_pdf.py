import asyncio
from pyppeteer import launch

async def to_pdf():

    options = {
        'landscape': False
    }

    browser = await launch({
        'headless': True,
        'devtools': False,
        # 'timeout': 1000 * 5,
        'dumpio': True,               # 解决浏览器多开卡死
        'autoClose': True,
        'ignoreDefaultArgs': ['--enable-automation'],
        # 'userDataDir': './userdata',
        'handleSIGINT': False,
        'handleSIGTERM': False,
        'handleSIGHUP': False,
        'ignoreHTTPSErrors': True,
        'args': [
            '--no-sandbox',  # --no-sandbox 在 docker 里使用时需要加入的参数，不然会报错
            '--disable-gpu',
            '--disable-extensions',
            '--disable-infobars',
            '--ignore-certificate-errors',
            '--ignore-ssl-errors',
            '--hide-scrollbars',
            '--disable-bundled-ppapi-flash',
            '--mute-audio',
            '--disable-setuid-sandbox',
            '--disable-xss-auditor',
            # '--single-process', # <- this one doesn't works in Windows
            # '--proxy-server=127.0.0.1:8080'
        ],
        'defaultViewport': {'width': 595, 'height': 842}
    })

    page = await browser.newPage()
    await page.goto(r'file:///E:/Desktop/The%20Very%20Hungry%20Caterpillar%20-%20Eric%20Carle/text/1.html', timeout=100)
    await page.emulateMedia('screen')

    dimensions = await page.evaluate('''() => {
        return {
            width: Math.max(
                document.body.scrollWidth,
                document.documentElement.scrollWidth,
                document.body.offsetWidth,
                document.documentElement.offsetWidth,
                document.documentElement.clientWidth
            ),
            height: Math.max(
                document.body.scrollHeight,
                document.documentElement.scrollHeight,
                document.body.offsetHeight,
                document.documentElement.offsetHeight,
                document.documentElement.clientHeight
            ),
            deviceScaleFactor: window.devicePixelRatio,
        }
    }''')

    # no idea while the dpi is 144, but not the 96 or even other else.
    print_pdf_dpi = 72 * 2

    if options['landscape']:
        px_w = print_pdf_dpi * 11.7
        px_h = print_pdf_dpi * 8.27
    else:
        px_w = print_pdf_dpi * 8.27
        px_h = print_pdf_dpi * 11.7

    scroll = 'x' if px_w / px_h < (dimensions['width'] / dimensions['height']) else 'y'

    if scroll == 'x':
        scale = px_w / dimensions['width']
    else:
        scale = px_h / dimensions['height']

    # do need to scale
    if scale < 1:
        scale = int(scale * 100)
        scale = scale / 100

    else:
        scale = 1

    await page.pdf({
        'path': 'test.pdf',
        'format': 'A4',
        'landscape': options['landscape'],
        'pageRanges': '1',
        'printBackground': True,
        'scale': scale
    })

    await browser.close()

if __name__ == '__main__':
    asyncio.run(to_pdf())