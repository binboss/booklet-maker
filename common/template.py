from common.env import APP_DIR
import os
import sys
from string import Template
if __name__ == '__main__':
    sys.path.append(os.path.abspath(os.path.join(
        os.path.dirname(__file__), os.path.pardir)))
sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(__file__), os.path.pardir)))


def get_single_page_template():
    with open(APP_DIR / 'template/one.template', 'r') as inf:
        single_page_template = Template(inf.read())

    return single_page_template


def get_new_book_two_panel_template():
    with open(APP_DIR / 'template/two.template', 'r') as inf:
        new_book_page_template = Template(inf.read())

    return new_book_page_template


def get_new_book_four_panel_template():
    with open(APP_DIR / 'template/four.template', 'r') as inf:
        new_book_page_template = Template(inf.read())

    return new_book_page_template


def get_new_book_eight_panel_template():
    with open(APP_DIR / 'template/eight.template', 'r') as inf:
        new_book_page_template = Template(inf.read())

    return new_book_page_template