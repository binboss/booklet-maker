import os
from pathlib import Path

CPU_NUM = os.cpu_count() or 1
APP_DIR = Path(__file__).parent.parent