#!/usr/bin/env python3
import os.path
import sys
import asyncio
from pyppeteer import launch
import tempfile
from ebooklib import epub
from string import Template
from loguru import logger

def do_coroutine(fn, *args, **kwargs):
    try:
        asyncio.get_event_loop().run_until_complete(fn(*args, **kwargs))
    except:
        logger.exception(f'An error occured while doing coroutine job {fn}')

async def to_image(url: str, outpath: str, viewport=None):
    try:
        if viewport is not None:
            browser = await launch(defaultViewport=viewport)
        else:
            browser = await launch()
        page = await browser.newPage()
        await page.goto(url, waitUntil='networkidle0')
        element = await page.J('body')
        await element.screenshot({'path': outpath, 'omitBackground': True})
        await browser.close()
    except:
        logger.exception('Failed to take screenshort.')

def save_book_files(book: epub.EpubBook):
    for item in book.items:
        if isinstance(item, epub.EpubHtml):
            continue

        file_name = item.file_name
        content = item.content

        # create needed directories
        dir_name = '{0}/{1}'.format(book_dir, os.path.dirname(file_name))
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)

        # print('>> {0}'.format(file_name))

        # write content to file
        with open('{0}/{1}'.format(book_dir, file_name), 'wb') as f:
            f.write(content)

def generate_new_book_page_html(images: list) -> str:
    new_book_page_template_src_vars = ['one', 'two', 'three', 'four']
    page_template_page = new_book_page_template.safe_substitute()
    img_tags = []
    for i, fname in enumerate(images):
        img_tags.append(f'<img src="{fname}" alt="comic page" class="{new_book_page_template_src_vars[i]} calibre2" />')

    page_template_page = Template(page_template_page).safe_substitute(
        img_tags = '\n'.join(img_tags)
    )

    return page_template_page

def generate_new_book_pages(page_type = 'front') -> str:
    logger.info(f'Generating new book {page_type} pages.')
    four_images = []
    page_num = 0
    total = len(images_in_order)
    if page_type == 'front':
        total_pages = int(total / 2 / 4 + 0.9)
    else:
        total_pages = int(total / 2 / 4)

    tempfile.tempdir = new_book_dir
    for (ii,(uid,fname)) in enumerate(images_in_order):
        fnum = ii + 1
        if page_type == 'front':
            if fnum % 2 == 0:
                # Even
                continue

        else:
            if not fnum % 2 == 0:
                # Even
                continue

        if len(four_images) < 4:
            four_images.append(fname)
            continue

        page_num += 1
        logger.info(f'Generating the {page_num} / {total_pages} page.')
        new_book_merge_images(four_images, page_type, f'{new_book_four_dir}/page_{page_type}_{page_num}.png')
        four_images.clear()
        four_images.append(fname)

    if len(four_images) > 0:
        logger.info(f'Generating the {total_pages} / {total_pages} page.')
        new_book_merge_images(four_images, page_type, f'{new_book_four_dir}/page_{page_type}_{total_pages}.png')

def new_book_order_images(images: list, page_type='front'):
    if page_type == 'front':
        return

    ordered_images = {}
    while len(images) < 4:
        images.append('blank.png')

    for i, image in enumerate(images):
        if (i + 1) % 2 == 0:
            # Even
            ordered_images[i - 1] = image
        else:
            ordered_images[i + 1] = image

    images.clear()
    images += [ordered_images[key] for key in sorted(ordered_images)]

def new_book_merge_images(images: list, page_type: str, outpath: str):
    new_book_order_images(images, page_type)
    page = generate_new_book_page_html(images)
    with tempfile.NamedTemporaryFile(suffix='.html', delete=True) as fp:
        fp.write(page.encode('utf-8'))
        fp.seek(0)
        do_coroutine(to_image, f'file://{fp.name}', outpath)

def get_spine_key(book):
    spine_keys = {id:(ii,id) for (ii,(id,show)) in enumerate(book.spine)}
    past_end = len(spine_keys)
    return lambda itm: spine_keys.get(itm.get_id(), (past_end,itm.get_id()))

if __name__ == '__main__':
    # read epub
    book = epub.read_epub(sys.argv[1])

    # get base filename from the epub
    book_dir = os.path.splitext(sys.argv[1])[0]
    script_dir = os.path.dirname(__file__)
    script_dir = script_dir if script_dir != '' else './'
    work_dir = os.path.dirname(book_dir)
    work_dir = work_dir if work_dir != '' else './'
    base_name = os.path.basename(book_dir)
    new_book_dir = f'{work_dir}/{base_name}_new'
    new_book_four_dir = f'{work_dir}/{base_name}_new/four'

    with open(f'{script_dir}/template/one.template', 'r') as inf:
        single_page_template = Template(inf.read())

    with open(f'{script_dir}/template/four.template', 'r') as inf:
        new_book_page_template = Template(inf.read())

    if not os.path.exists(new_book_dir):
        os.makedirs(new_book_dir)

    if not os.path.exists(new_book_four_dir):
        os.makedirs(new_book_four_dir)

    # save images
    save_book_files(book)

    # html to image with screenshot
    # logger.debug(sorted([get_spine_key(book)(itm) for itm in book.get_items()]))
    last_i = None
    skip_uids = ['titlepage', 'id1', 'id2', 'id3', 'cover', 'id68']
    images_in_order = []
    for key in sorted([get_spine_key(book)(itm) for itm in book.get_items()]):
        i = key[0]
        if i == last_i:
            break

        last_i = i
        uid = key[1]
        if uid in skip_uids:
            continue

        item = book.get_item_with_id(uid)
        logger.info(f'Converting page {uid} to image...')

        # html to image
        tempfile.tempdir = book_dir
        with tempfile.NamedTemporaryFile(suffix='.html', delete=True) as fp:
        # fp = tempfile.NamedTemporaryFile(suffix='.html')
            fp.write(item.content)
            fp.seek(0)
            fname = f'{uid}.png'
            do_coroutine(to_image, f'file://{fp.name}', f'{new_book_dir}/{fname}')

        # style the image with html template
        tempfile.tempdir = new_book_dir
        with tempfile.NamedTemporaryFile(suffix='.html', delete=True) as fp:
            single_page_page = single_page_template.safe_substitute(
                src = fname
            )
            fp.write(single_page_page.encode('utf-8'))
            fp.seek(0)
            do_coroutine(to_image, f'file://{fp.name}', f'{new_book_dir}/{fname}', viewport={'width':595, 'height':842})
            images_in_order.append(
                (uid, fname)
            )

    # generate a blank image
    do_coroutine(to_image, 'about:blank', f'{new_book_dir}/blank.png', viewport={'width':595, 'height':842})

    # for test: just make the last page do not have enough pages.
    # images_in_order = images_in_order[:-3]

    # logger.debug(images_in_order)
    generate_new_book_pages()
    generate_new_book_pages('back')