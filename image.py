import asyncio
import os
from typing import List, Union, Tuple, Optional
import pathlib
from pathlib import Path
import tempfile
from PIL import Image as ImagePIL, ImageDraw, ImageOps, ImageFont

# import imagesize


from utils import helper
from command import html_to_image
from common.template import get_single_page_template
from services.crop_service import CropService
from pdf import Pdf
from paper import *
from applib.policy_async import policy_async
from applib.log import logger


class Image(object):
    """..."""

    def __init__(self, fpath: Union[str, pathlib.PurePath], loop: asyncio.AbstractEventLoop = None) -> None:
        self.fpath = Path(fpath)
        self.fpath_str = str(self.fpath.resolve())
        self._image: ImagePIL.Image = None
        self.loop = loop or asyncio.get_event_loop()

    @property
    def image(self) -> ImagePIL.Image:
        if self._image is None:
            self._image = ImagePIL.open(self.fpath_str)

        return self._image

    def release(self) -> None:
        self.image.close()
        self._image = None

    def size(self):
        return self.image.size

    @staticmethod
    def ratio(size: Tuple[int, int]) -> float:
        (width, height) = size
        return width / height

    # get the new size after adding the border
    @staticmethod
    def get_size_after_border(size: Tuple[int, int], border: int, decrease: bool = False) -> Tuple[int, int]:
        """Get the new size after adding the border, aspect ratio is kept"""
        (width, height) = size
        ratio = Image.ratio(size)
        if ratio < 1:
            width = width - border * 2 if decrease else width + border * 2
            height = int(width / ratio)
        else:
            height = height - border * 2 if decrease else height + border * 2
            width = int(height * ratio)

        return (width, height)

    @staticmethod
    def fix_image_to_image(image: ImagePIL.Image, target: ImagePIL.Image) -> ImagePIL.Image:
        """Just make sure the image is not bigger than the target image
        """
        ratio = Image.ratio(image.size)
        while image.size[0] > target.size[0] or \
            image.size[1] > target.size[1] \
            :
            if image.size[0] > target.size[0]:
                image = image.resize(
                    (target.size[0], int(target.size[0] / ratio)),
                    resample=ImagePIL.LANCZOS)

            if image.size[1] > target.size[1]:
                image = image.resize(
                    (int(target.size[1] * ratio), target.size[1]),
                    resample=ImagePIL.LANCZOS)

        return image

    @staticmethod
    def paste_image_to_image(image: ImagePIL.Image, target: ImagePIL.Image, position: str = 'center', scale=False) -> ImagePIL.Image:
        """paste the image to the center, left or right of the target image, the position is based on the width.
        It will be always vertically center.

        return the new image"""
        if scale:
            (width_image, height_image) = image.size
            image = Image.fix_image_to_image(image, target)
            if width_image != image.size[0] or height_image != image.size[1]:
                logger.warning('The source image was scaled.')

        target = target.copy()
        (width, height) = target.size
        (width_image, height_image) = image.size
        if position == 'center':
            x = helper.round_half_up(width / 2 - width_image / 2)
        elif position == 'left':
            x = 0
        else:
            x = width - width_image

        y = helper.round_half_up(height / 2 - height_image / 2)

        target.paste(image, (x, y))
        return target

    @staticmethod
    def paste_image_to_center(image: ImagePIL.Image, target: ImagePIL.Image, scale=False) -> ImagePIL.Image:
        """paste the image to the center of the target image, return the new image"""
        return Image.paste_image_to_image(image, target, 'center', scale=scale)

    @staticmethod
    def paste_image_to_left(image: ImagePIL.Image, target: ImagePIL.Image, scale=False) -> ImagePIL.Image:
        """paste the image to the left of the target image, return the new image"""
        return Image.paste_image_to_image(image, target, 'left', scale=scale)

    @staticmethod
    def paste_image_to_right(image: ImagePIL.Image, target: ImagePIL.Image, scale=False) -> ImagePIL.Image:
        """paste the image to the right of the target image, return the new image"""
        return Image.paste_image_to_image(image, target, 'right', scale=scale)

    async def resize_async(self, width: int = None, height: int = None) -> ImagePIL.Image:
        return policy_async(self.resize, width, height, loop=self.loop)

    async def blank_async(self) -> ImagePIL.Image:
        return await policy_async(self.blank, loop=self.loop)

    @staticmethod
    async def blank_image_async(width: int, height: int) -> ImagePIL.Image:
        return await policy_async(Image.blank_image, width, height, loop=asyncio.get_event_loop())

    @staticmethod
    def save(image: ImagePIL.Image, output: str, *args, **kwargs) -> None:
        ''' save image to output file
        '''
        image.save(output, *args, quality=95, subsampling=0, **kwargs)

    async def crop_async(self, output_folder: str = None) -> Union[str, ImagePIL.Image]:
        ''' crop the image
        '''
        # return await self.crop_from_pdf(output_folder)
        return await policy_async(self.crop, output_folder, loop=self.loop)

    async def split_async(self, output_folder: str = None) -> List[str]:
        ''' split landscape image into two pieces
        '''
        return await policy_async(self.split, output_folder, loop=self.loop)

    def resize(self, width: int = None, height: int = None) -> ImagePIL.Image:
        if width is None and height is None:
            raise ValueError("Either 'width' or 'height' argument should " +
                             "be passed")
        if height is None:
            height = int(width / ratio(self.size()))
        if width is None:
            width = int(height * ratio(self.size()))

        image = self.image.resize((width, height), resample=ImagePIL.LANCZOS)
        self.release()

        return image

    def paste_to_preset_page(self,
        preset: str = 'A4',
        dpi: int = None,
        scale=True,
        align: str = 'center',
        landscape=False,
        caption: str = None,
        caption_font: str = 'arial.ttf',
        caption_font_size: int = 10,
        caption_margin: Tuple[int, int, int, int] = (0, 0, 0, 0),
        paper_size: Tuple[int, int] = None,
        margin: Tuple[int, int] = (0, 0),
        border: int = 0,
        custom_preset_size: Tuple[int, int] = None,
    ) -> ImagePIL.Image:
        ''' convert the image to a4, and return the Image

        :param dpi: custom dpi for the A4 artifact image, e.g. 300. if None, use the image's self dpi
        :param scale: if scale the image to the A4 artifact size, only works when custom dpi is set
        :param landscape: if the artifact is landscape
        :param align: center, left, right
        :param caption: if set, add a caption to the image
        :param caption_font: the font for the caption
        :param caption_font_size: the font size for the caption
        :param caption_margin: if set, add margin to the caption
        :param paper_size: Use to scale the font size, if None, use the preset size
        :param margin: if set, add margin to the image
        :param border: if set, add border to the image. while caption is added, the border will be added to the image that contains the caption
        :param custom_preset_size: if set, use the custom preset size instead of the preset size

        return the Image
        '''
        image = self.image.copy()
        self.release()
        ratio = self.ratio(image.size)

        paper_cls = get_paper_class(preset)
        preset_page_ratio = paper_cls.preset_ratio

        # Create an empty white image with the size of specified preset
        # output_width, output_height = paper_cls.to_preset_size(image.size, dpi=dpi, landscape=landscape)
        if custom_preset_size is not None:
            output_width, output_height = custom_preset_size
        else:
            output_width, output_height = paper_cls.preset_size_by_dpi(dpi)

        paper_size = (output_width, output_height) if paper_size is None else paper_size

        if landscape:
            output_width, output_height = output_height, output_width
            preset_page_ratio = 1 / preset_page_ratio

        output_preset_image = ImagePIL.new('RGB', (output_width, output_height), (255, 255, 255))
        output_preset_image_bordered = output_preset_image.copy() # The new image size after adding the border
        if border > 0:
            _width, _height = Image.get_size_after_border(output_preset_image_bordered.size, border, decrease=True)
            output_preset_image_bordered = output_preset_image.resize((_width, _height), resample=ImagePIL.LANCZOS)

        # Paste the image into the preset (e.g: A4) image

        # scale the image to fit the preset image
        if dpi and scale:
            if self.ratio(image.size) > preset_page_ratio:
                image = image.resize(
                    (output_preset_image_bordered.size[0], int(output_preset_image_bordered.size[0] / ratio)),
                    resample=ImagePIL.LANCZOS)

            else:
                image = image.resize(
                    (int(output_preset_image_bordered.size[1] * ratio), output_preset_image_bordered.size[1]),
                    resample=ImagePIL.LANCZOS)

        else:
            # Just make sure the image is not bigger than the preset image
            image = self.fix_image_to_image(image, output_preset_image_bordered)

        logger.debug(f'scaled: output_width: {output_width}, output_height: {output_height}, image.size: {image.size}')

        # add margin and border to the image
        # always make sure add the specified margin and border to the image, so the image may need to be resized.
        if any(margin) or border > 0:
            resize_width = image.size[0] - margin[0] * 2
            _resize_height = resize_width / ratio
            resize_height = image.size[1] - margin[1] * 2
            if _resize_height > resize_height:
                resize_height = int(_resize_height)

            else:
                resize_width = int(resize_height * ratio)

            image = image.resize((resize_width, resize_height), resample=ImagePIL.LANCZOS)

            # add border top and bottom only if no caption is set
            if not caption:
                image = self.paste_image_to_image(image, output_preset_image_bordered, align)
                # Image.save(image, self.fpath.parent / f'{self.fpath.stem}_resize_for_no_caption.jpg') # for debug

        logger.debug(f'margin or border added: output_width: {output_width}, output_height: {output_height}, image.size: {image.size}')

        # Add caption
        if caption is not None:
            # make caption_margin modifiable
            caption_margin = list(caption_margin)
            image_with_caption = output_preset_image_bordered.copy()

            # font size should be calculated based on the image size as the font size is based on dpi 96
            # font = ImageFont.truetype(caption_font, int(caption_font_size * dpi / 96))
            a4_preset_size = get_paper_class('A4').preset_size_by_dpi(dpi)
            font_size = int(caption_font_size * dpi / 96 * paper_size[0] / a4_preset_size[0])
            font = ImageFont.truetype(caption_font, font_size)
            draw = ImageDraw.Draw(image_with_caption)
            # Get the dimensions of the text for dynamic placement
            text_w, text_h = draw.textsize(caption, font=font)

            # There is an invisible top offet around the text.
            # ImageDraw.textsize() returns incorrect textsize for truetype fonts
            # https://github.com/python-pillow/Pillow/issues/644
            # https://stackoverflow.com/questions/59008322/pillow-imagedraw-text-coordinates-to-center/59008967#59008967
            # offset_x, offset_y = font.getoffset(caption)
            text_bbox = font.getbbox(caption)

            # text location
            text_loc = (int((image_with_caption.size[0] - text_w) / 2), image_with_caption.size[1] - text_h - caption_margin[3])
            # Get the image margin, both left and right, top and bottom have the same value.
            image_margin = (int((image_with_caption.size[0] - image.size[0]) / 2), int((image_with_caption.size[1] - image.size[1]) / 2))
            # Check whether current margin (with border) is enough to place the caption margin.
            # If enough, use the current margin.
            image_bbox = Image.get_bbox_of_white_border(image) or (0, 0, 0, 0)
            image_border = (image_bbox[0], image_bbox[1], image.size[0] - image_bbox[2], image.size[1] - image_bbox[3])
            caption_margion_top_curr = image_margin[1] + image_border[3] - caption_margin[3] - text_h
            if caption_margion_top_curr > caption_margin[1]:
                caption_margin[1] = 0
            else:
                caption_margin[1] -= caption_margion_top_curr

            # text block size with it's margins, the height should not contain the invisible top offet text_bbox[1]
            text_block_size = (text_w + caption_margin[0] + caption_margin[2], text_h + caption_margin[1] + caption_margin[3] - text_bbox[1])
            # The image may need to be resized smaller to fit the caption with margin
            image_new_height = image_with_caption.size[1] - text_block_size[1] - image_margin[1]
            if image_new_height < image.size[1]:
                width = int(image_new_height * ratio)
                image = image.resize((width ,image_new_height), resample=ImagePIL.LANCZOS)
                # Image.save(image, self.fpath.parent / f'{self.fpath.stem}_resize_for_caption.png') # for debug

            # paste the image
            image_with_caption = self.paste_image_to_image(image, image_with_caption, align)
            # Draw the text
            draw = ImageDraw.Draw(image_with_caption)
            draw.text(text_loc, caption, font=font, fill=(0, 0, 0))
            image.close()
            # Image.save(image_with_caption, self.fpath.parent / f'{self.fpath.stem}_caption.png') # for debug
            image = image_with_caption

        # the image should lie in the center of the original image
        output_preset_image = self.paste_image_to_image(image, output_preset_image, align)
        image.close()

        # validate the border
        # im_inverted = ImageOps.invert(output_preset_image)
        # bbox = im_inverted.getbbox()
        # if bbox:
        #     logger.debug(f'{border} => {bbox[0]}, {output_preset_image.size[0] - bbox[2]}')

        return output_preset_image


    def blank(self) -> ImagePIL.Image:
        blank_image = self.image.copy()
        (width, height) = self.size()
        blank_image.paste((255, 255, 255), (0, 0, width, height))

        return blank_image

    @staticmethod
    def blank_image(width: int, height: int) -> ImagePIL.Image:
        blank_image = ImagePIL.new('RGB', (width, height), (255, 255, 255))
        return blank_image

    def get_processed_image_path(self, action: str, output_folder: str = None) -> str:
        ''' Get the processed image path
        '''
        if output_folder is None:
            output_folder: Path = self.fpath.parent
        else:
            output_folder = Path(output_folder)

        new_image = str((output_folder / f'{self.fpath.stem}_{action}{self.fpath.suffix}').resolve())
        return new_image


    async def to_page(self, landscape=False) -> str:
        ''' style the image to page

        :param landscape: if the image is landscape

        return the image file name
        '''
        pass

    async def to_page_by_html(self, landscape=False) -> str:
        ''' style the image with html template
        '''
        single_page_template = get_single_page_template()
        tempfile.tempdir = self.fpath.parent
        fp = tempfile.NamedTemporaryFile(suffix='.html', delete=False)
        try:
            template_vars = dict(
                src=self.fpath_str,
                landscape='landscape' if landscape else ''
            )
            single_page_page = single_page_template.safe_substitute(
                src=template_vars['src'],
                landscape=template_vars['landscape']
            )

            fp.write(single_page_page.encode('utf-8'))
            fp.close()
            # image = html_to_image.to_image(fp.name, fname, need_crop=False, viewport={
            #                                'width': 595, 'height': 842})
            image = (await html_to_image.to_image(fp.name, self.fpath, need_crop=False, landscape=landscape, auto_close=False))[0]
            return image

        finally:
            os.unlink(fp.name)
            # pass

    # def crop(self, output_folder: str = None) -> str:
    #     ''' crop the image
    #     '''
    #     threshold: int = 255
    #     dark_background_light_foreground = False
    #     if threshold < 0:
    #         threshold = -threshold
    #         dark_background_light_foreground = True

    #     im = ImagePIL.open(self.fpath).convert('RGB')

    #     if not dark_background_light_foreground:
    #         im = im.point(lambda p: 255 if p < threshold else 0) # create negative image
    #     else:
    #         im = im.point(lambda p: 255 if p >= threshold else 0) # create positive image

    #     Image.save(im, Path(output_folder) / f'{self.fpath.stem}_test{self.fpath.suffix}')
    #     exit(-1)

    #     # Calculate the bounding box of the negative image, and append to list.
    #     bounding_box = calculate_bounding_box_from_image(im)
    #     trimmed: ImagePIL.Image = im.crop(bounding_box)

    #     # Save
    #     if output_folder is None:
    #         output_folder = self.fpath.parent

    #     output_path = Path(output_folder) / f'{self.fpath.stem}_cropped{self.fpath.suffix}'
    #     Image.save(trimmed, output_path)

    #     return str(output_path.resolve())

    @staticmethod
    def add_border(im: ImagePIL.Image, border: int, color: tuple = (0, 0, 0)) -> ImagePIL.Image:
        ''' add border to the image
        '''
        _im = im.copy()
        width, height = Image.get_size_after_border(_im.size, border, decrease=False)
        new_im = ImagePIL.new('RGB', (width, height), color)
        x = helper.round_half_up((width - _im.size[0]) / 2)
        y = helper.round_half_up((height - _im.size[1]) / 2)
        new_im.paste(_im, (x, y))
        _im.close()
        return new_im

    @staticmethod
    def get_bbox_of_white_border(im: ImagePIL.Image) -> Optional[tuple]:
        ''' get the bounding box of the white border
        '''
        im = im.copy()
        # invert image (so that white is 0)
        im_inverted = ImageOps.invert(im)
        # Get bounding box of text
        bbox = im_inverted.getbbox()
        im_inverted.close()
        im.close()

        return bbox

    @staticmethod
    def auto_remove_border(im: ImagePIL.Image) -> ImagePIL.Image:
        """Remove the border of the image, return the image without border but with the same aspect ratio
        """
        ratio = Image.ratio(im.size)
        width, height = im.size

        # get the bounding box of the white border
        bbox = Image.get_bbox_of_white_border(im)
        if bbox is None:
            return im.copy()

        # trim image
        bbox = list(bbox)
        bbox[0] = min(bbox[0], width - bbox[2])
        bbox[2] = width - bbox[0]
        bbox[1] = min(bbox[1], height - bbox[3])
        bbox[3] = height - bbox[1]
        im = im.crop(bbox)

        # expand image
        width, height = im.size

        if Image.ratio((width, height)) > ratio:
            # use width as reference, expand height
            height = int(width / ratio)
        else:
            # use height as reference, expand width
            width = int(height * ratio)

        image = ImagePIL.new('RGB', (width, height), (255, 255, 255))
        x = helper.round_half_up((width - im.size[0]) / 2)
        y = helper.round_half_up((height - im.size[1]) / 2)
        image.paste(im, (x, y))
        im.close()

        return image

    @staticmethod
    def remove_transparency(image: ImagePIL.Image) -> ImagePIL.Image:
        ''' remove transparency
        '''
        if image.mode == 'RGB':
            return image

        image = image.convert('RGBA')
        background = ImagePIL.new('RGBA', image.size, (255, 255, 255))
        background.paste(image, mask=image.split()[-1])
        image.close()

        # remove alpha channel
        background = background.convert('RGB')

        return background

    def crop(self, output_folder: str = None) -> Union[str, ImagePIL.Image]:
        ''' crop the image

        :param output_folder: the output folder. If None, return the image object

        return the image file name or the image object
        '''

        trimmed = self.crop_image(self.image)
        self.release()

        # Save
        if output_folder is None:
            # output_folder = self.fpath.parent
            return trimmed

        output_path = self.get_processed_image_path('cropped', output_folder)
        Image.save(trimmed, output_path)

        return output_path


    @staticmethod
    def crop_image(im: ImagePIL.Image) -> ImagePIL.Image:
        """..."""
        _im = im.copy()
        # remove transparency
        _im = Image.remove_transparency(_im)

        # invert image (so that white is 0)
        im_inverted = ImageOps.invert(_im)

        # Get bounding box of text and trim to it
        bbox = im_inverted.getbbox()
        im_inverted.close()
        # print(bbox)
        # Image.save(background, Path(output_folder) / f'{self.fpath.stem}_test{self.fpath.suffix}')
        trimmed: ImagePIL.Image = _im.crop(bbox)
        _im.close()

        return trimmed


    async def crop_from_pdf(self, output_folder: str = None) -> str:
        ''' crop the image
        '''
        pdf_file = Path(output_folder) / f'{self.fpath.stem}.pdf'
        pdf_file = str(pdf_file.resolve())
        await self.to_pdf_async(pdf_file)
        pdf = Pdf(pdf_file)
        await CropService.crop_queue.put(pdf_file)
        await pdf.wait_for_min_pdf()

        pdf = Pdf(pdf.min_pdf_path)
        images = await pdf.to_images_async(fmt='png', output_folder=output_folder, paths_only=True)
        image = images[0]

        return image


    def split(self, output_folder: str = None) -> List[str]:
        ''' split landscape image into two pieces
        '''
        tiles = []
        img = ImagePIL.open(self.fpath)
        width, height = img.size
        if width > height:
            if output_folder is None:
                output_folder = str(self.fpath.parent.resolve())

            # math.cell(width / 2)
            #(lambda x: x if x == int(x) else int(x)+1)(width / 2)
            tiles = helper.tile(self.fpath.name, dir_in=output_folder, dir_out=output_folder, d=int(width / 2), height=height, ignore_partial=False, keep_source=True)

        if len(tiles) == 0:
            tiles.append(self.fpath_str)

        return tiles


    async def to_pdf_async(self, output_folder: str = None) -> str:
        ''' convert image to pdf
        '''
        return await policy_async(self.to_pdf, output_folder, loop=self.loop)

    def to_pdf(self, output_file: str) -> str:
        ''' convert the image to pdf
        '''
        im = ImagePIL.open(self.fpath)
        Image.save(im, output_file)

        return output_file


def calculate_bounding_box_from_image(im: ImagePIL.Image) -> Tuple[int, int, int, int]:
    """This function uses a Pillow routine to get the bounding box, in bp, of
    the rendered image."""
    x_max, y_max = im.size
    bounding_box = im.getbbox() # Note this uses ltrb convention.
    if not bounding_box:
        #print("\nWarning: could not calculate a bounding box for this page."
        #      "\nAn empty page is assumed.", file=sys.stderr)
        bounding_box = (x_max/2, y_max/2, x_max/2, y_max/2)

    bounding_box = list(bounding_box) # Make temporarily mutable.

    # Compensate for reversal of the image y convention versus PDF.
    bounding_box[1] = y_max - bounding_box[1]
    bounding_box[3] = y_max - bounding_box[3]

    return tuple(bounding_box)


def almost_equal(a, b):
    return abs(a - b) < 0.1

def ratio(size):
    (width, height) = size
    return width / height

def square(size):
    (width, height) = size
    return width * height