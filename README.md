`python main.py book.epub -p 4`

## Docker
`sudo docker run -it --rm -v $(pwd):/usr/src/app/book comic-maker  /usr/src/app/book/mybook.pdf -p 2 -l -c --cutter --saddle`