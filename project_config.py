import os
from dataclasses import dataclass, field, fields
from typing import List, Tuple, Dict
from pathlib import Path
from concurrent.futures import ThreadPoolExecutor
from common.env import CPU_NUM

@dataclass
class ProjectConfig:
    """..."""
    book_path: Path
    book_name: str = None
    book_type: str = None
    work_dir: Path = None
    book_extract_dir: Path = None
    new_book_src_dir: Path = None
    panel: int = 2
    dpi: int = 300
    need_crop: bool = field(default=False)
    save_only: bool = field(default=False)
    landscape: bool = field(default=False)
    split: bool = field(default=False)
    parts: int = 1
    parts_reuse_first: bool = field(default=False)
    parts_reuse_last: bool = field(default=False)
    parts_no_reset_pagination: bool = field(default=False)
    no_pagination: bool = field(default=False)
    pagination_font: str = 'arial.ttf'
    pagination_font_size: int = 12
    pagination_starts_from: int = 1
    pagination_prefix: str = ''
    pagination_suffix: str = ''
    pagination_margin: Tuple[int, int, int, int] = field(default_factory=lambda: (0, 36, 0, 0))
    margin: Tuple[int, int] = field(default_factory=lambda: (0, 0))
    border: int = 6 # The border will be add to the resized image.
    bookbinding_width: int = 20
    bookbinding_widthless: bool = False
    printer_border: int = None # May be updated while init(), value from self.dpi * self.printer_border_in_inches
    printer_borderless: bool = field(default=False)
    printer_border_in_inches: float = 0.12
    # printer_border_in_inches: float = 0.25
    saddle: bool = field(default=False)
    cutter: bool = field(default=False)
    last_after_blank: bool = field(default=False)
    blank_after_first: bool = field(default=False)
    first_always_blank: bool = field(default=False)
    last_always_blank: bool = field(default=False)
    target_paper_type: str = 'A4'
    output_page_extension: str = '.jpg'
    skip_uids: List[str] = field(default_factory=lambda: [])
    max_workers: int = CPU_NUM
    executor: ThreadPoolExecutor = None
    _sorted_config: Dict = None


    def __post_init__(self):
        self.pagination_margin = tuple(self.pagination_margin)
        self.margin = tuple(self.margin)
        if self.printer_borderless:
            self.printer_border = 0
        elif self.printer_border is None:
            self.printer_border = int(self.dpi * self.printer_border_in_inches)

        if self.book_path.is_dir():
            self.book_name = self.book_path.name
        else:
            self.book_name = self.book_path.stem

        # self.book_name = os.path.basename(os.path.splitext(self.book_path)[0])
        self.book_type = self.book_path.suffix.lower()
        if self.book_path.is_file() and self.book_type not in ('.pdf', '.epub'):
            import magic
            m = magic.from_file(str(self.book_path.resolve()), mime=True)
            if len(m.split('/')) == 2:
                t, subt = m.split('/')
                self.book_type = f'.{subt}'

        self.work_dir = self.book_path.parent if not self.work_dir else Path(self.work_dir)
        self.work_dir.mkdir(parents=True, exist_ok=True)
        self.book_extract_dir = self.work_dir / self.book_name
        self.new_book_src_dir = self.work_dir / f'{self.book_name}_new'

    # sort the config by key
    @property
    def sorted_config(self):
        if self._sorted_config is not None:
            return self._sorted_config

        sorted_config = {}
        for field in sorted(fields(self), key=lambda f: f.name):
            value = getattr(self, field.name)
            if isinstance(value, os.PathLike):
                sorted_config[field.name] = str(Path(value).resolve())
            else:
                sorted_config[field.name] = value

        sorted_config.pop('_sorted_config', None)
        self._sorted_config = sorted_config
        return self._sorted_config

def init(**kwargs):
    """..."""
    global config
    config = ProjectConfig(**kwargs)

def dump():
    """..."""
    print(config)

config: ProjectConfig = None