FROM --platform=$TARGETPLATFORM python:3.9-slim
ARG TARGETPLATFORM
ARG BUILDPLATFORM

WORKDIR /usr/src/app

# install runtime dependencies
# Install latest chrome dev package and fonts to support major charsets (Chinese, Japanese, Arabic, Hebrew, Thai and a few others)
# Note: this installs the necessary libs to make the bundled version of Chromium that Puppeteer
# installs, work.
RUN set -eux; \
    \
	apt-get update -y; \
    apt install -y --no-install-recommends \
        gnupg \
        curl \
    ;
RUN set -eux; \
    \
    curl -fsSL https://dl.google.com/linux/linux_signing_key.pub | gpg --dearmor | tee /usr/share/keyrings/google-chrome-archive-keyring.gpg > /dev/null; \
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/google-chrome-archive-keyring.gpg] https://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list; \
    apt-get update -y; \
    apt install -y --no-install-recommends \
        # browser
        google-chrome-stable \
        fonts-ipafont-gothic \
        fonts-wqy-zenhei \
        fonts-thai-tlwg \
        fonts-kacst \
        fonts-freefont-ttf \
        libxss1 \
        # pdfCropMargins
        ghostscript \
        poppler-utils \
	; \
	rm -rf /var/lib/apt/lists/*

COPY requirements.txt requirements.txt

RUN set -eux; \
    \
    pip install -r requirements.txt

COPY . .

ENV BROWSER_BIN=/usr/bin/google-chrome-stable

ENTRYPOINT ["python", "main.py"]
CMD ["--help"]