import pdfCropMargins.external_program_calls
# from pdfCropMargins.pdfCropMargins import CapturingTextStream
from pdfCropMargins.external_program_calls import remove_program_temp_directory
import threading


# Override to prevent duplicated thread starting
def _uninterrupted_remove_program_temp_directory():
    pass
    # _t = threading.Thread(target=remove_program_temp_directory, args=())
    # _t.start()
    # _t.join()


pdfCropMargins.external_program_calls.uninterrupted_remove_program_temp_directory = _uninterrupted_remove_program_temp_directory