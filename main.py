#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import asyncio
from pathlib import Path
from typing import Union
import argparse
import signal
# import functools
from concurrent.futures.process import BrokenProcessPool
from contextlib import suppress
from natsort import os_sorted
from common.env import CPU_NUM
from pdf import Pdf
if sys.platform == 'win32':
    import win_unicode_console
    win_unicode_console.enable()

import project_config
from sourceType.epub import EpubType
from sourceType.pdf import PdfType
from sourceType.folder import FolderType
from page import Page, Pages
from panel.base_panel import BasePanel
from panel.two_panel import TwoPanel
from panel.four_panel import FourPanel
from panel.eight_panel import EightPanel
from panel.sixteen_panel import SixteenPanel
from applib.log import logger
debug, info, warn, error, fatal = logger.debug, logger.info, logger.warning, logger.error, logger.critical
# class GracefulExit(SystemExit):
#     code = 1


# def raise_graceful_exit(*args):
#     tasks = asyncio.all_tasks(loop=loop)
#     for t in tasks:
#         t.cancel()

#     loop.stop()
#     print("Gracefully shutdown")
#     raise GracefulExit()


def keyboardInterruptHandler(signal, frame):
    # 如果用sys.exit()会报系统异常达不到直接结束程序的效果。
    # 因为sys.exit()触发的是系统异常，Exception只能捕获由代码触发的异常。
    # 即使如此也不推荐使用sys.exit来结束进程，
    # 因为Ctrl-C退出的时候需要按三次才能完全退出（和进程数无关）

    print("KeyboardInterrupt (ID: {}) has been caught. Cleaning up...".format(signal))
    os._exit(0)


class Formatter(object):

    def __init__(self, config: project_config.ProjectConfig = None):
        if config:
            self.config = config
            project_config.config = self.config

        else:
            self.config = project_config.config

        self.images_in_order = []

    async def run(self):
        if self.config.book_path.is_dir():
            pass

        elif self.config.book_path.is_file():
            if self.config.book_type not in ['.pdf', '.epub']:
                error('Unsupport file type.')
                return False

        else:
            error('Invalid book file or directory.')
            return False

        self.config.new_book_src_dir.mkdir(exist_ok=True)

        filetype: Union[EpubType, PdfType, FolderType] = None

        if self.config.book_path.is_file():
            self.config.book_extract_dir.mkdir(exist_ok=True)

            if self.config.book_type == '.epub':
                filetype = EpubType()
                # filetype_args['skip_uids'] = ['titlepage', 'id1', 'id2', 'id3', 'cover', 'id68']
            else:
                filetype = PdfType()

        else:
            filetype = FolderType()

        info('Saving book files...')
        await filetype.save_book_files()

        info('Loading images...')
        images_in_order = await filetype.get_images_in_order()
        if len(images_in_order) == 0:
            error('No images found.')
            return False

        info('Decoding pages...')
        _pages = [Page(i+1, image) for i, image in enumerate(images_in_order)]
        pages = Pages(_pages, max_workers=self.config.max_workers)
        pages = await pages.decorate(output_folder=self.config.new_book_src_dir, dpi=self.config.dpi, need_crop=self.config.need_crop, split=self.config.split, landscape=self.config.landscape)
        # info(self.images_in_order)

        # for test: just make the last page do not have enough pages.
        # self.images_in_order = self.images_in_order[:-2]

        if self.config.save_only:
            info('Just saving the images...')

            if isinstance(filetype, FolderType):
                for f in os_sorted(self.config.book_path.glob('*.pdf')):
                    f: Path = f
                    pdf=Pdf(f)
                    await pdf.to_images_async(self.config.new_book_src_dir,
                        thread_count=self.config.max_workers, use_cropbox=True, output_file=f'{f.stem}-', jpegopt={"quality": 100, "progressive": False, "optimize": False}, fmt='jpeg', dpi=300, size=(2481, None))

            info('done')
            return

        info('Generating book for panel...')

        pannel: Union[BasePanel, TwoPanel, FourPanel, EightPanel] = None
        if self.config.panel == 4:
            pannel = FourPanel(pages=pages)
        elif self.config.panel == 8:
            pannel = EightPanel(pages=pages)
        elif self.config.panel == 16:
            pannel = SixteenPanel(pages=pages)
        else:
            pannel = TwoPanel(pages=pages)


        await pannel.generate_new_book_pages_for_parts()

        info('done')


def main():

    signal.signal(signal.SIGINT, keyboardInterruptHandler) # 捕获singint信号，交给ctrl_c处理
    signal.signal(signal.SIGTERM, keyboardInterruptHandler)

    parser = argparse.ArgumentParser(
        description='Formatting ebook to two or four panel comic')
    parser.add_argument('book', type=str, nargs='+',
                        help='The book file or ordered images location')
    parser.add_argument('-p', '--panel', type=int, dest='panel',
                        default=2,
                        nargs='?',
                        help='The panel mode (two/four) to format to')
    parser.add_argument('-c', '--crop', dest='need_crop',
                        action='store_true',
                        help='Crop the image. Only working in pdf or folder.')
    parser.add_argument('-l', '--landscape', dest='landscape',
                        action='store_true',
                        help='Set the orientation to landscape. default is "horizontal"')
    parser.add_argument('-s', '--split', dest='split',
                        action='store_true',
                        help='Split the image into two. It is useful while you think there are two pages in singl one page. Only working while orientation is "horizontal"')
    parser.add_argument('-pt', '--parts', type=int, dest='parts',
                        default=1,
                        help='Split the book to parts')
    parser.add_argument('-ptrf', '--parts-reuse-first', dest='parts_reuse_first',
                        action='store_true',
                        help='Reuse the first page of the book as the first page of the parts')
    parser.add_argument('-ptrl', '--parts-reuse-last', dest='parts_reuse_last',
                        action='store_true',
                        help='Reuse the last page of the book as the last page of the parts')
    parser.add_argument('-ptnrpa', '--parts-no-reset-pagination', dest='parts_no_reset_pagination',
                        action='store_true',
                        help='Do not reset the pagination of the parts, so the paging starts from --pagination-starts-from (which default is 1) again')
    parser.add_argument('-d', '--dpi', dest='dpi',
                        type=int,
                        default=300,
                        help='The dpi of the image. default is 300')
    parser.add_argument('-npag', '--no-pagination', dest='no_pagination',
                        action='store_true',
                        help='Disable pagination')
    parser.add_argument('-pagf', '--pagination-font', dest='pagination_font',
                        type=str,
                        default='arial.ttf',
                        help='The font of the pagination. default is "arial.ttf"')
    parser.add_argument('-pagfs', '--pagination-font-size', dest='pagination_font_size',
                        type=int,
                        default=12,
                        help='The font size of the pagination. default is 10. Based on the Word size "小五号", 0.125inch * 96dpi = 12')
    parser.add_argument('-pagst', '--pagination-starts-from', dest='pagination_starts_from',
                        type=int,
                        default=1,
                        help='The start page of pagination, default is 1. only works when --add-pagination is enabled')
    parser.add_argument('-pagpf', '--pagination-prefix', dest='pagination_prefix',
                        type=str,
                        default='',
                        help='The prefix of pagination. default is empty string. only works when --add-pagination is enabled')
    parser.add_argument('-pagsf', '--pagination-suffix', dest='pagination_suffix',
                        type=str,
                        default='',
                        help='The suffix of pagination. default is empty string. only works when --add-pagination is enabled')
    parser.add_argument('-pagmg', '--pagination-margin', nargs='+', dest='pagination_margin',
                        type=int,
                        default=(0, 36, 0, 0),
                        help='The margin of pagination. default is (0, 36, 0, 0) based on dpi 300 (11.52 / 96 * 300)')
    parser.add_argument('-mg', '--margin', nargs='+', dest='margin',
                        type=int,
                        default=(0, 0),
                        help='Add margin in pixel to the original image (left, right) while is resizing. default is (0, 0). Example: --margin 0 10, or --margin 10 10')
    parser.add_argument('--border', dest='border',
                        type=int,
                        default=6,
                        help='Add border to the original page while is resizing. default is 6. Example: --border 0, or --border 6')
    parser.add_argument('-bdw', '--bookbinding-width', dest='bookbinding_width',
                        type=int,
                        default=20,
                        help='The width of the bookbinding area. default is 20')
    parser.add_argument('-bdwl', '--bookbinding-widthless', dest='bookbinding_widthless',
                        action='store_true',
                        help='Remove the bookbinding width, you may also want to set --border to 0, and without the -c (--crop) to keep the original style.')
    parser.add_argument('-so', '--save-only', dest='save_only',
                        action='store_true',
                        help='Only save the images.')
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-prbr', '--printer-border', dest='printer_border',
                        type=int,
                        # default=36,
                        help='The border of the printer. default is None. Calculate by the dpi * printer_border_in_inches. The 36 is based on 0.12 inch (3.048 mm) with 300 dpi. Example: --printer-border 0, --printer-border 36 (0.12 inch), or --printer-border 75 (0.25 inch')
    def IntorFloat(string):
        if '.' in string: return float(string)
        else: return int(string)
    group.add_argument('-prbrin', '--printer-border-in-inches', dest='printer_border_in_inches',
                        type=IntorFloat,
                        default=0.12,
                        help='The printer border in inches. Will be used to calculate the printer border (--printer-border). When --printer-border is provided, this option will be Ineffective anymore. Default is 0.12')
    parser.add_argument('-prbl', '--printer-borderless', dest='printer_borderless',
                        action='store_true',
                        help='Enable the borderless mode. Default is False. While True, the printer_border will be ignored (just set to 0).')
    parser.add_argument('-sd', '--saddle', dest='saddle',
                        action='store_true',
                        help='Saddle pages.')
    parser.add_argument('-cr', '--cutter', dest='cutter',
                        action='store_true',
                        help='Cutter mode.')
    parser.add_argument('-labl', "--last-after-blank", action="store_true",
                        help="If additional blank page(s) are needed, then move the last page after the blank page(s).")
    parser.add_argument('-blaf', "--blank-after-first", action="store_true",
                        help="insert additional blank page after the first page")
    parser.add_argument('-fbl', "--first-always-blank", action="store_true",
                        help='Always insert blank pages before first page')
    parser.add_argument('-lbl', "--last-always-blank", action="store_true",
                        help='Always insert blank pages after last one.')
    parser.add_argument('-w', '--max-workers', type=int, dest='max_workers',
                        default=CPU_NUM,
                        help='Override the default max workers which according to the cpu cores.')
    parser.add_argument('--target-paper-type', dest='target_paper_type',
                        type=str,
                        default='A4',
                        help='The target paper type. default is A4')
    args = parser.parse_args()
    if args.panel not in [2, 4, 8, 16]:
        parser.error("panel can only be 2, 4 or 8")


    if sys.platform == 'win32':
        warn("D'oh! UVLoop is not support Windows!")
        loop = asyncio.ProactorEventLoop()
    else:
        import uvloop
        loop = uvloop.new_event_loop()

    # Initial the project config
    book_path = Path(args.book[0])
    args_dict = args.__dict__
    args_dict.pop('book')
    project_config.init(book_path=Path(book_path), **args_dict)

    # dump the config
    project_config.dump()

    asyncio.set_event_loop(loop)

    # signal.signal(signal.SIGINT,  raise_graceful_exit)
    # signal.signal(signal.SIGTERM, raise_graceful_exit)

    formatter = Formatter()

    try:
        info("  --> Start main loop.")
        loop.run_until_complete(formatter.run())
    except (KeyboardInterrupt, RuntimeError, BrokenProcessPool):
        info("  < Stopping loop.")
        pass
    except:
        logger.exception("  < Error occured.")
    finally:
        loop.stop()
        pending = asyncio.all_tasks(loop=loop)
        for task in pending:
            task.cancel()
            with suppress(asyncio.CancelledError):
                loop.run_until_complete(task)
        info("   <- Main loop stopped.")
    loop.close()
    info("  <-- Main loop closed.")

if __name__ == "__main__":
    main()
