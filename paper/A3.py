"""..."""
from paper import Paper

class A3(Paper):
    width = 842
    height = 1191
    _dpi = 72
    name = 'A3'
    preset = 'A3'
    _size = (width, height)
    _ratio = width / height
    _ratio_in_landscape = height / width
    size_in_inches = (11.69, 16.54)
    size_in_mm = (297, 420)
    size_in_cm = (29.7, 41.0)
    size_in_pt = (297 * 72, 420 * 72)
    size_in_px = (297 * 96, 420 * 96)
    size_in_pc = (297 * 12, 420 * 12)
    size_in_em = (297 * 12, 420 * 12)
    size_in_ex = (297 * 12, 420 * 12)