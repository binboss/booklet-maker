""".."""
from paper import Paper

class A5(Paper):
    width = 420
    height = 595
    _dpi = 72
    name = 'A5'
    preset = 'A5'
    _size = (width, height)
    _ratio = width / height
    _ratio_in_landscape = height / width
    size_in_inches = (5.83, 8.27)
    size_in_mm = (148, 210)
    size_in_cm = (14.8, 21.0)
    size_in_pt = (148 * 72, 210 * 72)
    size_in_px = (148 * 96, 210 * 96)
    size_in_pc = (148 * 12, 210 * 12)
    size_in_em = (148 * 12, 210 * 12)
    size_in_ex = (148 * 12, 210 * 12)