"""..."""
from paper import Paper

class A8(Paper):
    width = 147
    height = 210
    _dpi = 72
    name = 'A8'
    preset = 'A8'
    _size = (width, height)
    _ratio = width / height
    _ratio_in_landscape = height / width
    size_in_inches = (2.04, 2.92)
    size_in_mm = (52, 74)
    size_in_cm = (5.2, 7.4)
    size_in_pt = (52 * 72, 74 * 72)
    size_in_px = (52 * 96, 74 * 96)
    size_in_pc = (52 * 12, 74 * 12)
    size_in_em = (52 * 12, 74 * 12)
    size_in_ex = (52 * 12, 74 * 12)