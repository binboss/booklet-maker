"""..."""
from typing import Union
from .paper import Paper
from .A0 import A0
from .A1 import A1
from .A2 import A2
from .A3 import A3
from .A4 import A4
from .A5 import A5
from .A6 import A6
from .A7 import A7
from .A8 import A8

PaperType = Union[Paper, A0, A1, A2, A3, A4, A5, A6, A7, A8]

def get_paper_class(preset: str) -> 'PaperType':
    if preset == 'A0':
        return A0
    elif preset == 'A1':
        return A1
    elif preset == 'A2':
        return A2
    elif preset == 'A3':
        return A3
    elif preset == 'A4':
        return A4
    elif preset == 'A5':
        return A5
    elif preset == 'A6':
        return A6
    elif preset == 'A7':
        return A7
    elif preset == 'A8':
        return A8
    else:
        raise ValueError('Unknown preset: ' + preset)
