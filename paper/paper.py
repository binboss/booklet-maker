"""..."""
import os
import sys
from typing import (
    Callable,
    Generic,
    Tuple,
    TypeVar,
)

if __name__ == '__main__':
    sys.path.append(os.path.abspath(os.path.join(
        os.path.dirname(__file__), os.path.pardir)))
sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(__file__), os.path.pardir)))

from utils import helper


T = TypeVar('T')


class classproperty(Generic[T]):
    """Converts a method to a class property.
    """

    def __init__(self, f: Callable[..., T]):
        self.fget = f

    def __get__(self, instance, owner) -> T:
        return self.fget(owner)


class Paper:
    """Must initialize this class with the method from_dpi()

    e.g.
    a4 = A4.from_dpi(300)
    """
    # Just initalize with the attributes of A4 for type checking
    width = 595
    height = 842
    _dpi = 72
    name = 'A4'
    preset = 'A4'
    _size = (width, height)
    _ratio = width / height
    _ratio_in_landscape = height / width
    size_in_inches = (8.27, 11.69)
    size_in_mm = (210, 297)
    size_in_cm = (21.0, 29.7)
    size_in_pt = (210 * 72, 297 * 72)
    size_in_px = (210 * 96, 297 * 96)
    size_in_pc = (210 * 12, 297 * 12)
    size_in_em = (210 * 12, 297 * 12)
    size_in_ex = (210 * 12, 297 * 12)

    def __init__(self):
        pass

    @property
    def name(self) -> str:
        return self.preset

    @property
    def preset(self) -> str:
        return self.name

    @property
    def size(self) -> Tuple[int, int]:
        return self.width, self.height

    @property
    def size_in_half(self) -> Tuple[int, int]:
        return helper.round_half_up(self.width / 2), helper.round_half_up(self.height / 2)

    @property
    def dpi(self) -> int:
        return self._dpi

    @dpi.setter
    def dpi(self, dpi: int) -> None:
        self.width, self.height = self.size_by_dpi(dpi)
        self._dpi = dpi

    @property
    def ratio(self) -> float:
        """..."""
        return self.get_ratio(self.width, self.height)

    @property
    def ratio_in_landscape(self) -> float:
        """..."""
        return 1 / self.ratio

    @staticmethod
    def get_ratio(width: int, height: int) -> float:
        """..."""
        return width / height

    @classproperty
    def preset_dpi(cls) -> int:
        """..."""
        return cls._dpi

    @classproperty
    def preset_size(cls) -> Tuple[int, int]:
        """..."""
        return cls._size

    @classproperty
    def preset_ratio(cls) -> float:
        """..."""
        return cls._ratio

    @classproperty
    def preset_ratio_in_landscape(cls) -> float:
        """..."""
        return cls._ratio_in_landscape

    @classmethod
    def preset_size_by_dpi(cls, dpi: int) -> Tuple[int, int]:
        """calcalate the resolution by speific dpi and size"""
        # width, height = cls.width * dpi / cls._dpi, cls.height * dpi / cls._dpi
        width, height = cls.size_in_mm[0] / 25.4 * dpi, cls.size_in_mm[1] / 25.4 * dpi # in inches, 1 inch = 25.4 mm
        return helper.round_half_up(width), helper.round_half_up(height)

    def scale_size_in_half(self, times: int = 1) -> Tuple[int, int]:
        """scale the size in times of half"""
        return helper.round_half_up(self.width / 2 / times), helper.round_half_up(self.height / 2 / times)

    def scale_size_to_preset(self, preset: str) -> Tuple[int, int]:
        """Manually scale the size to speical preset"""
        diff = int(self.preset[1:]) - int(preset[1:])
        if diff == 0:
            return self.size

        width, height = self.size
        if diff < 0:
            for i in range(-diff):
                _height = width
                _width = height / 2
                width, height = _width, _height
        else:
            for i in range(diff):
                _width = height
                _height = width * 2
                width, height = _width, _height

        return helper.round_half_up(width), helper.round_half_up(height)

    def size_by_dpi(self, dpi: int) -> Tuple[int, int]:
        """calcalate the resolution by speific dpi and size"""
        # length_w, length_h = self.size_in_mm[0] / 25.4, self.size_in_mm[1] / 25.4 # in inches, 1 inch = 25.4 mm
        width, height = self.width * dpi / self.dpi, self.height * dpi / self.dpi
        return helper.round_half_up(width), helper.round_half_up(height)

    def scale_px_by_dpi(self, target_px: int, px: int, dpi: int = 300) -> int:
        """scale the px by dpi, default use the A4 with dpi 300 as the reference"""
        base_px = self.size_by_dpi(dpi)[0]
        return self.scale_px(target_px, px, base_px)

    @staticmethod
    def scale_px(target_px: int, px: int, base_px: int) -> int:
        if px == 0:
            return 0

        return helper.round_half_up(target_px / base_px * px)

    @classmethod
    def to_preset_size(cls, size: Tuple[int, int], dpi: int = None, landscape: bool = False) -> Tuple[int, int]:
        """Scale to preset size, dpi is used to calculate the size of the preset page"""
        width, height = size
        present_page_ratio = cls.preset_ratio if not landscape else cls.preset_ratio_in_landscape
        # Just not use the cls.size_in_inches, because the size_in_inches is not accurate enough
        present_page_size_in_inches = [size / 25.4 for size in cls.size_in_mm]
        if not dpi:
            # Use the image's dpi
            if width / height > present_page_ratio:
                height = int(width / present_page_ratio)
            else:
                width = int(height * present_page_ratio)

        else:
            preset_page_width, preset_page_height = (int(dpi * present_page_size_in_inches[0]), int(dpi * present_page_size_in_inches[1]))
            if landscape:
                preset_page_width, preset_page_height = preset_page_height, preset_page_width

            # choose the larger one
            if width / height > present_page_ratio:
                width = max(preset_page_width, width)
                height = int(width / present_page_ratio)
            else:
                height = max(preset_page_height, height)
                width = int(height * present_page_ratio)

        return width, height

    @classmethod
    def from_dpi(cls, dpi: int, border: int = 0) -> 'Paper':
        paper = cls()
        paper.width -= border * 2
        paper.height -= border * 2
        paper.dpi = dpi
        return paper