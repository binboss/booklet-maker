"""..."""
from paper import Paper

class A7(Paper):
    width = 210
    height = 298
    _dpi = 72
    name = 'A7'
    preset = 'A7'
    _size = (width, height)
    _ratio = width / height
    _ratio_in_landscape = height / width
    size_in_inches = (2.92, 4.14)
    size_in_mm = (74, 105)
    size_in_cm = (7.4, 10.5)
    size_in_pt = (10 * 72, 12 * 72)
    size_in_px = (10 * 96, 12 * 96)
    size_in_pc = (10 * 12, 12 * 12)
    size_in_em = (10 * 12, 12 * 12)
    size_in_ex = (10 * 12, 12 * 12)