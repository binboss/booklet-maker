"""..."""
from paper import Paper

class A1(Paper):
    width = 1684
    height = 2384
    _dpi = 72
    name = 'A1'
    preset = 'A1'
    _size = (width, height)
    _ratio = width / height
    _ratio_in_landscape = height / width
    size_in_inches = (23.39, 33.11)
    size_in_mm = (594, 841)
    size_in_cm = (59.4, 84.1)
    size_in_pt = (594 * 72, 841 * 72)
    size_in_px = (594 * 96, 841 * 96)
    size_in_pc = (594 * 12, 841 * 12)
    size_in_em = (594 * 12, 841 * 12)
    size_in_ex = (594 * 12, 841 * 12)