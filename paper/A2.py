"""..."""
from paper import Paper

class A2(Paper):
    width = 1191
    height = 1684
    _dpi = 72
    name = 'A2'
    preset = 'A2'
    _size = (width, height)
    _ratio = width / height
    _ratio_in_landscape = height / width
    size_in_inches = (16.54, 23.39)
    size_in_mm = (420, 594)
    size_in_cm = (41.0, 59.4)
    size_in_pt = (420 * 72, 594 * 72)
    size_in_px = (420 * 96, 594 * 96)
    size_in_pc = (420 * 12, 594 * 12)
    size_in_em = (420 * 12, 594 * 12)
    size_in_ex = (420 * 12, 594 * 12)