"""..."""
from paper import Paper

class A6(Paper):
    width = 298
    height = 420
    _dpi = 72
    name = 'A6'
    preset = 'A6'
    _size = (width, height)
    _ratio = width / height
    _ratio_in_landscape = height / width
    size_in_inches = (4.14, 5.83)
    size_in_mm = (105, 148)
    size_in_cm = (10.5, 14.8)
    size_in_pt = (105 * 72, 148 * 72)
    size_in_px = (105 * 96, 148 * 96)
    size_in_pc = (105 * 12, 148 * 12)
    size_in_em = (105 * 12, 148 * 12)
    size_in_ex = (105 * 12, 148 * 12)