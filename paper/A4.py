"""..."""
from paper import Paper

class A4(Paper):
    width = 595
    height = 842
    _dpi = 72
    name = 'A4'
    preset = 'A4'
    _size = (width, height)
    _ratio = width / height
    _ratio_in_landscape = height / width
    size_in_inches = (8.27, 11.69)
    size_in_mm = (210, 297)
    size_in_cm = (21.0, 29.7)
    size_in_pt = (210 * 72, 297 * 72)
    size_in_px = (210 * 96, 297 * 96)
    size_in_pc = (210 * 12, 297 * 12)
    size_in_em = (210 * 12, 297 * 12)
    size_in_ex = (210 * 12, 297 * 12)


if __name__ == '__main__':

    a4 = A4.from_dpi(300)
    print(a4)