"""..."""
from paper import Paper

class A0(Paper):
    width = 2384
    height = 3370
    _dpi = 72
    name = 'A0'
    preset = 'A0'
    _size = (width, height)
    _ratio = width / height
    _ratio_in_landscape = height / width
    size_in_inches = (33.11, 46.81)
    size_in_mm = (841, 1189)
    size_in_cm = (84.1, 118.9)
    size_in_pt = (841 * 72, 1189 * 72)
    size_in_px = (841 * 96, 1189 * 96)
    size_in_pc = (841 * 12, 1189 * 12)
    size_in_em = (841 * 12, 1189 * 12)
    size_in_ex = (841 * 12, 1189 * 12)