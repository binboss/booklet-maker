import asyncio
from threading import Thread
from pathlib import Path
from pdf import Pdf
from applib.log import logger

class CropService(object):
    """..."""
    crop_queue: asyncio.Queue = None


    def __init__(self) -> None:
        pass

    async def wait_for_min_pdf(pdfpath: Path) -> None:
        min_pdf_path = pdfpath.parent / f'{pdfpath.stem}_min.pdf'
        while not min_pdf_path.is_file():
            await asyncio.sleep(1)
            logger.debug('  Waiting for the cropped pdf.')
            continue

        pdfpath.unlink()
        min_pdf_path.rename(pdfpath)


    async def crop_queue_service(self, main_loop):
        while True:
            try:
                r = asyncio.run_coroutine_threadsafe(self.crop_queue.get(), main_loop)
                rs: str = r.result()
                pdf = Pdf(rs)
                await pdf.crop_async()

                await asyncio.sleep(0)

            except Exception:
                logger.exception('Failed to get item from crop queue.')


    def start_crop_queue_loop(self, main_loop):
        loop = asyncio.new_event_loop()
        asyncio.run_coroutine_threadsafe(self.crop_queue_service(main_loop), loop=loop)
        loop.run_forever()


def start_crop_queue_in_thread():
    CropService.crop_queue = asyncio.Queue()
    crop_service = CropService()
    t= Thread(target=crop_service.start_crop_queue_loop, args=(asyncio.get_event_loop(),))
    t.setDaemon(True)
    t.start()