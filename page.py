from __future__ import annotations
import asyncio
from typing import List, Tuple
from pathlib import Path
from PIL import Image as ImagePIL
import numpy as np
import shutil

# import imagesize
from common.env import CPU_NUM
from common.concurrent import add_task, ordinary_tasks
from utils import helper
from applib.policy_async import policy_async
from image import Image
from applib.interrupt import KeyboardInterruptBlocked
from applib.log import logger
debug, info, warn, error = logger.debug, logger.info, logger.warning, logger.error


class Page(object):
    """..."""
    def __init__(self, id: int, image_path: str, parent=None, position=None, is_blank=False, loop: asyncio.AbstractEventLoop = None):
        self.id = id
        self.image = Image(image_path, loop=loop)
        self.parent = parent
        self.position = position
        self.is_blank = is_blank
        self.loop = loop or asyncio.get_event_loop()
        debug("new page: " + str(self))

    def __repr__(self):
        return f'Page({self.id}, {self.image.fpath})'

    async def paste_to_preset_page_async(self, *args, **kwargs) -> Page:
        """..."""
        # out: Path = self.image.get_processed_image_path('A4')
        # kwargs.pop('keep_source', None)
        # a4_image = self.image.paste_to_a4(*args, **kwargs)
        # Image.save(a4_image, out)
        # return self
        return await policy_async(self.paste_to_preset_page, *args, loop=self.loop, **kwargs)

    def paste_to_a4_page(self, *args, dpi: int = 300, landscape=False, keep_source=True, **kwargs) -> Page:
        return self.paste_to_preset_page(*args, preset='A4', dpi=dpi, landscape=landscape, keep_source=keep_source, **kwargs)

    def paste_to_preset_page(self, *args, preset: str = 'A4', dpi: int = 300, landscape=False, keep_source=True, **kwargs) -> Page:
        """..."""
        out: Path = self.image.get_processed_image_path(preset)
        image_in_preset_page_size = self.image.paste_to_preset_page(*args, preset=preset, dpi=dpi, landscape=landscape, **kwargs)
        # image_in_preset_page_size = ImagePIL.open(self.image.fpath_str)
        Image.save(image_in_preset_page_size, out)
        page = Page(self.id, out, parent=self.parent, position=self.position, is_blank=self.is_blank, loop=self.loop)
        if not keep_source:
            self.image.fpath.unlink()

        return page


    def rotate(self, angle: int) -> Page:
        """..."""
        image = self.image.image.rotate(angle, expand=True)
        out = self.image.get_processed_image_path('rotated')
        Image.save(image, out)
        self.image.release()
        page = Page(self.id, out, parent=self.parent, position=self.position, is_blank=self.is_blank, loop=self.loop)
        return page


    def transpose(self, method: str) -> Page:
        """..."""
        image = self.image.image.transpose(method)
        out = self.image.get_processed_image_path('transposed')
        Image.save(image, out)
        self.image.release()
        page = Page(self.id, out, parent=self.parent, position=self.position, is_blank=self.is_blank, loop=self.loop)
        return page

    async def resize_async(self, width: int = None, height: int = None, expand: bool = True, align: str = 'center', keep_source=True) -> Page:
        """...."""
        return await policy_async(self.resize, width=width, height=height, expand=expand, align=align, keep_source=keep_source, loop=self.loop)

    def resize(self, width: int = None, height: int = None, expand: bool = True, align: str = 'center', keep_source=True) -> Page:
        """Resize page

        Args:
            expand: expand, if True, the page will be expanded to the same size of the largest page with white background.
            align: expand bases on center, left or right
        """
        fpath = self.image.fpath
        out: Path = self.image.get_processed_image_path('resized')
        im = Image(fpath, loop=self.loop)
        if expand:
            # create new image and paste to the center
            new_image = ImagePIL.new('RGB', (width, height), (255, 255, 255))
            new_image = Image.paste_image_to_center(im.image, new_image, scale=True)
            Image.save(new_image, out)
            im.release()

        else:
            im_resized: ImagePIL.Image = im.resize(width=width, height=height)
            Image.save(im_resized, out)

        if not keep_source:
            fpath.unlink()

        page = Page(self.id, out, parent=self.parent, position=self.position, is_blank=self.is_blank, loop=self.loop)
        return page

    async def decorate(self, output_folder: str, dpi: int = 300, need_crop=False, split=False, landscape=False) -> List[Page]:
        """Decorate page save to output_folder

        Args:
            output_folder: output folder
            need_crop: need crop
            split: split page
            landscape: landscape page

        Returns:
            List[Page]

        """
        pages: List[Page] = []

        if need_crop:
            new_image = await self.image.crop_async(output_folder)
                    # Just copy the image to the output folder
        else:
            new_image = Path(output_folder) / Path(self.image.fpath).name
            shutil.copy(self.image.fpath, new_image)

        img = Image(new_image)

        # split landscape image into two pieces
        tiles = []
        if split and not landscape:
            tiles = await img.split_async(output_folder)

        if len(tiles) == 0:
            tiles.append(new_image)

        for i,t in enumerate(tiles):
            # img = Image(t)
            # new_image = await img.to_page(landscape=landscape)
            # Path(t).unlink()
            page = Page(self.id + (i+1) / 10, t, parent=self.parent, position=self.position, is_blank=self.is_blank, loop=self.loop)
            pages.append(page)

        return pages

class Pages(object):
    """..."""
    def __init__(self, pages: List[Page], max_workers: int = 0, loop: asyncio.AbstractEventLoop = None) -> None:
        """..."""
        self.pages = pages
        self.max_workers = max_workers if max_workers > 0 else CPU_NUM

        # 创建一个 asyncio.Semaphore 实例，最多允许激活 max_concur_req 个使用这个计数器的协程
        self.semaphore = asyncio.Semaphore(self.max_workers)
        self.loop = loop or asyncio.get_event_loop()


    # Return the length of the pages
    def __len__(self):
        return len(self.pages)

    def __repr__(self) -> str:
        return f'Pages({self.pages})'

    async def decorate(self, output_folder: str, dpi: int = 300, need_crop=False, split=False, landscape=False) -> Pages:
        """Decorate page save to output_folder
        The Page.id will be changed to the new id.

        Args:
            output_folder: output folder
            dpi: dpi
            need_crop: need crop
            split: split page
            landscape: landscape page

        Returns:
            Pages

        """
        logger.info("Decorate pages")

    # async def decorate(self, semaphore: asyncio.Semaphore, output_folder: str, index=0, need_crop=False, split=False, landscape=False) -> Tuple[Tuple[Union[int, float], str]]:
        # semaphore 参数是 asyncio.Semaphore 类的实例
        # Semaphore 类是同步装置，用于限制并发请求

        # dst: Dict[int, Page] = {}
        dst: List[Page] = []

        tasks: List[asyncio.Task] = []
        for index, page in enumerate(self.pages):
            logger.info(f'Decorating the number {index} image')
            with KeyboardInterruptBlocked():
                task = await add_task(self.semaphore, page.decorate, output_folder, dpi=dpi, need_crop=need_crop, split=split, landscape=landscape)
                tasks.append(task)
                await asyncio.sleep(0)

        async for task in ordinary_tasks(tasks):
            pages: List[Page] = await task
            for index, page in enumerate(pages):
                # key = (task.idx) * 10 + index
                # key = int(page.id * 10) + index
                # dst[key] = page
                dst.append(page)

        # pages = Pages([dst[key] for key in sorted(dst.keys())], self.max_workers, self.loop)
        dst.sort(key=lambda x: x.id)
        pages = Pages(dst, self.max_workers, self.loop)
        return pages

    async def resize(self, expand: bool = True, align: str = 'center', max_size: Tuple[int, int] = None, size_ignore_pages: Tuple[int] = None, keep_source=True) -> Pages:
        """Resize page to the same size of the largest page.
        The Page.id will be changed to the new id.
        The original order of the pages will be kept.

        Args:
            expand: expand, if True, the page will be expanded to the same size of the largest page with white background.
            align: expand bases on center, left, right, oddleftevenright or oddrightevenleft
        """
        info("Resize pages")
        total_pages = len(self.pages)
        dst = np.empty(total_pages, dtype=object)

        width = 0
        height = 0
        # pages_width_list = []
        # pages_height_list = []
        pages_ratio_list = []
        size_ignore_pages = size_ignore_pages or []
        for index, page in enumerate(self.pages):
            if index in size_ignore_pages:
                continue
            size = page.image.size()
            pages_ratio_list.append(Image.ratio(size))
            width = max(width, size[0])
            height = max(height, size[1])
            # pages_width_list.append(size[0])
            # pages_height_list.append(size[1])

        if max_size:
            # most pages are landscape?
            max_width, max_heigth = max_size
            if np.median(pages_ratio_list) > 1:
                max_heigth, max_width = max_size

            width = min(width, max_width)
            height = min(height, max_heigth)

        # width = helper.round_half_up(np.median(pages_width_list))
        # height = helper.round_half_up(np.median(pages_height_list))

        tasks: List[asyncio.Task] = []
        for index, page in enumerate(self.pages):
            logger.info(f'Adding the resize task for the number {index+1} / {total_pages} image')
            with KeyboardInterruptBlocked():
                align_ = align
                if align in ('oddleftevenright', 'oddrightevenleft'):
                    align_ = 'left' if align == 'oddleftevenright' else 'right'
                    align_ = align_ if (index+1) % 2 > 0 else list(filter(lambda i: i!=align_, ('left', 'right')))[0]

                task = await add_task(self.semaphore, page.resize_async, width=width, height=height, expand=expand, align=align_, keep_source=keep_source)
                tasks.append(task)
                await asyncio.sleep(0)

        async for task in ordinary_tasks(tasks):
            page: Page = await task
            dst[task.idx - 1] = page

        info("Resize pages done")
        return Pages(dst.tolist(), self.max_workers, self.loop)


    async def resize_to_preset_page(self, *args,
        preset: str = 'A4',
        dpi: int = 300,
        landscape: bool = False,
        align: str = 'center',
        keep_source=True,
        add_pagination=False,
        pagination_starts_from: int = 1,
        pagination_prefix: str = '',
        pagination_suffix: str = '',
        ignore_first_pagination=True,
        ignore_last_pagination=True,
        ignore_blank_pagination=True,
        border: int = 0,
        caption_margin: Tuple = (0, 0, 0, 0),
        margin: Tuple = (0, 0),
        **kwargs
    ) -> Pages:
        """Resize pages to A4 size.

        :param align: avaliable options: center, left, right, oddleftevenright, oddrightevenleft
        Return the new pages with the new id but the same order as the old pages.
        """
        logger.info(f"Resize pages to {preset} page size")

        total_pages = len(self.pages)
        dst = np.empty(total_pages, dtype=object)

        tasks: List[asyncio.Task] = []
        total_pages = len(self.pages)
        for index, page in enumerate(self.pages):
            logger.info(f'Adding the resize task for the number {index+1} / {total_pages} image')
            with KeyboardInterruptBlocked():
                caption = None
                if add_pagination and \
                    not (page.is_blank and ignore_blank_pagination) and \
                    not (index == 0 and ignore_first_pagination) and \
                    not (ignore_last_pagination and (index == total_pages - 1 or self.pages[index+1].is_blank)):
                        caption = pagination_starts_from+index
                        caption = pagination_prefix + str(caption) + pagination_suffix if caption > 0 else None
                        caption_margin = caption_margin if caption is not None else (0, 0, 0, 0)

                align_ = align
                if align_ in ('oddleftevenright', 'oddrightevenleft'):
                    align_ = 'left' if align == 'oddleftevenright' else 'right'
                    align_ = align_ if (index+1) % 2 > 0 else list(filter(lambda i: i!=align_, ('left', 'right')))[0]

                task = await add_task(
                    self.semaphore,
                    page.paste_to_preset_page_async,
                    *args,
                    preset=preset,
                    dpi=dpi,
                    scale=True,
                    caption=caption,
                    caption_margin=caption_margin,
                    margin=margin,
                    border=border,
                    landscape=landscape,
                    align=align_,
                    keep_source=keep_source,
                    **kwargs)
                tasks.append(task)
                await asyncio.sleep(0)

        async for task in ordinary_tasks(tasks):
            page: Page = await task
            dst[task.idx - 1] = page

        info("Resize pages to A4 done")
        return Pages(dst.tolist(), self.max_workers, self.loop)
