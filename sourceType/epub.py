import os
import sys
import asyncio
from typing import List
from pathlib import Path
from ebooklib import epub
import shutil
import tempfile
from pyppeteer import launch
from pyppeteer.browser import Browser
from collections import OrderedDict

if __name__ == '__main__':
    sys.path.append(os.path.abspath(os.path.join(
        os.path.dirname(__file__), os.path.pardir)))
sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(__file__), os.path.pardir)))

from command import html_to_image
from common.concurrent import add_task, ordinary_tasks
from applib.log import logger
from applib.interrupt import KeyboardInterruptBlocked
from sourceType.base import BaseType


class EpubType(BaseType):

    def __init__(self):
        super(EpubType, self).__init__()

        self.book = epub.read_epub(os.fspath(self.config.book_path))
        self.skip_uids: List[str] = self.config.skip_uids

    def get_spine_key(self):
        spine_keys = {id: (ii, id)
                      for (ii, (id, show)) in enumerate(self.book.spine)}
        past_end = len(spine_keys)
        return lambda itm: spine_keys.get(itm.get_id(), (past_end, itm.get_id()))

    async def save_book_files(self):
        shutil.rmtree(self.config.book_extract_dir, ignore_errors=True)
        self.config.book_extract_dir.mkdir(parents=True, exist_ok=True)
        for item in self.book.items:
            if isinstance(item, epub.EpubHtml):
                continue

            file_name = item.file_name
            content = item.content

            # print('>> {0}'.format(file_name))
            fpath = (self.config.book_extract_dir / file_name)
            fpath_parts = list(fpath.parts)
            if 'OEBPS' in fpath_parts:
                fpath_parts.remove('OEBPS')

            fpath = Path(*fpath_parts)
            fpath.parent.mkdir(parents=True, exist_ok=True)

            # write content to file
            with open(self.config.book_extract_dir / fpath, 'wb') as f:
                f.write(content)

    async def page_to_image(self, item: epub.EpubHtml, fname: str) -> List[Path]:
        # html to image
        tempfile.tempdir = (self.config.book_extract_dir / item.get_name()).parent
        tempfile.tempdir.mkdir(parents=True, exist_ok=True)

        fp = tempfile.NamedTemporaryFile(suffix='.html', delete=False)
        try:
#                       html += b'''<style>
# body, body * {
#     width: fit-content;
#     height: fit-content;
# }
# </style>
# '''
            fp.write(item.content)
            # fp.seek(0)
            fp.close()
            # fname = f'{index}.png'
            images = await html_to_image.to_image(
                fp.name, f'{self.config.book_extract_dir}/{fname}', auto_close=False)
            return images[0]

        except:
            logger.exception('Failed to parse the page to image.')

        finally:
            os.unlink(fp.name)
            # pass


    async def get_images_in_order(self) -> List[str]:
        # html to image with screenshot
        # logger.warning(sorted([self.get_spine_key()(itm) for itm in self.book.get_items()]))

        dst: OrderedDict[int, str] = OrderedDict()

        browser: Browser = await launch(options=html_to_image.BROWSER['options'])
        html_to_image.BROWSER['instance'] = browser

        # if self.need_crop:
        #     start_crop_queue_in_thread()

        # 创建一个 asyncio.Semaphore 实例，最多允许激活 max_concur_req 个使用这个计数器的协程
        semaphore = asyncio.Semaphore(self.config.max_workers)

        tasks: List[asyncio.Task] = []
        last_i = None
        for key in sorted([self.get_spine_key()(itm) for itm in self.book.get_items()]):
            try:
                with KeyboardInterruptBlocked():
                    i = key[0]
                    if i == last_i:
                        break

                    last_i = i
                    uid = key[1]
                    if uid in self.skip_uids:
                        continue

                    item: epub.EpubHtml = self.book.get_item_with_id(uid)
                    if not isinstance(item, epub.EpubHtml):
                        continue

                    if not item.is_chapter():
                        continue

                    logger.info(f'Adding the `page_to_image` task for the number {last_i} page')
                    task = await add_task(semaphore, self.page_to_image, item, f'{i+1}.png')
                    tasks.append(task)
                    await asyncio.sleep(0)

            except:
                logger.exception('Failed to get the image in order.')

        async for task in ordinary_tasks(tasks):
            image_path: str = await task
            dst[task.idx - 1] = image_path

        # cleaning
        await browser.close()

        return list(dst.values())