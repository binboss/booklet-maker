import os
import sys
import asyncio
from typing import List, Union
from pathlib import Path
import tempfile
from pyppeteer import launch
from pyppeteer.browser import Browser
# import imagesize
import shutil
from natsort import os_sorted

if __name__ == '__main__':
    sys.path.append(os.path.abspath(os.path.join(
        os.path.dirname(__file__), os.path.pardir)))
sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(__file__), os.path.pardir)))

from sourceType.base import BaseType
from applib.io import copytree
from applib.log import logger


class FolderType(BaseType):

    def __init__(self):
        super(FolderType, self).__init__()

    async def save_book_files(self) -> None:
        if self.config.book_path == self.config.book_extract_dir:
            logger.info('No need to save book files for folder source type')
            return

        shutil.rmtree(self.config.book_extract_dir, ignore_errors=True)
        await copytree(self.config.book_path, self.config.book_extract_dir, dirs_exist_ok=True)

    async def get_images_in_order(self, skip_indexes: List = []) -> List[str]:
        logger.info('Getting the decorated images in order')

        images_in_order: List[str] = []
        for f in os_sorted(self.config.book_extract_dir.glob('*')):
            if not f.suffix.lower().endswith(('.jpg', '.png')):
                continue

            images_in_order.append(str(f.resolve()))

        return images_in_order