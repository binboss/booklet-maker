import os
import sys
from typing import List

if __name__ == '__main__':
    sys.path.append(os.path.abspath(os.path.join(
        os.path.dirname(__file__), os.path.pardir)))
sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(__file__), os.path.pardir)))

import project_config

class BaseType(object):

    def __init__(self):
        self.config = project_config.config

    async def save_book_files(self, *args, **kwargs) -> None:
        pass

    async def get_images_in_order(self, *args, **kwargs) -> List[str]:
        raise NotImplementedError()