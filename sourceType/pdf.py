import os
import sys
from typing import List, Union
import shutil
# import imagesize

if __name__ == '__main__':
    sys.path.append(os.path.abspath(os.path.join(
        os.path.dirname(__file__), os.path.pardir)))
sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(__file__), os.path.pardir)))

import project_config
from pdf import Pdf
from paper import A4
from common.env import CPU_NUM
from sourceType.base import BaseType
from sourceType.folder import FolderType


class PdfType(BaseType):

    def __init__(self):
        super(PdfType, self).__init__()
        self.config = project_config.config

    async def save_book_files(self):
        pdf = Pdf(self.config.book_path)
        shutil.rmtree(self.config.book_extract_dir, ignore_errors=True)
        self.config.book_extract_dir.mkdir(parents=True, exist_ok=True)
        await pdf.to_images_async(
            output_folder=self.config.book_extract_dir,
            thread_count=CPU_NUM,
            use_cropbox=True,
            output_file='pic-',
            jpegopt={"quality": 100, "progressive": False, "optimize": False},
            fmt='jpeg',
            dpi=self.config.dpi,
            size=(A4.preset_size_by_dpi(self.config.dpi)[0], None)
        )

    async def get_images_in_order(self, skip_indexes: List = []) -> List[str]:
        folder_type = FolderType()

        images_in_order = await folder_type.get_images_in_order()

        return images_in_order
