import os
import asyncio
from typing import Union, List
from pathlib import Path
from pdf2image import convert_from_path, pdfinfo_from_path
from pdfCropMargins import crop
import numpy as np
from common.env import CPU_NUM
from applib.policy_async import policy_async
from applib.log import logger


class Pdf:
    def __init__(self, pdfpath: Union[str, Path], loop: asyncio.AbstractEventLoop = None):
        self.pdfpath = Path(pdfpath)
        self._min_pdf_path = self.pdfpath.parent / f'{self.pdfpath.stem}_min.pdf'
        self.loop = loop or asyncio.get_event_loop()


    async def crop_async(self) -> None:
        """Crop the PDF
        """

        await policy_async(self.crop, loop=self.loop)

    def crop(self) -> None:
        min_pdf_path = self.pdfpath.parent / f'{self.pdfpath.stem}_min.pdf'
        crop(["-p", "0", "-t", "255", os.fspath(self.pdfpath), '-o', os.fspath(min_pdf_path)])
        # pdfpath.unlink()
        # min_pdf_path.rename(pdfpath)

    @property
    def min_pdf_path(self):
        if not self._min_pdf_path.is_file():
            return None

        return self._min_pdf_path

    async def wait_for_min_pdf(self) -> None:
        while not self.min_pdf_path:
            await asyncio.sleep(1)
            logger.debug('Waiting for the cropped pdf.')
            continue

        # self.pdfpath.unlink()
        # self.min_pdf_path.rename(self.pdfpath)

    async def to_images_async(self, output_folder=None, **kwargs) -> List[str]:
        """Convert the PDF to images
        """

        return await policy_async(self.to_images, output_folder=output_folder, loop=self.loop, **kwargs)

    def to_images(self, output_folder=None, **kwargs) -> List[str]:
        """Convert the PDF to images
        """

        info = pdfinfo_from_path(self.pdfpath, userpw=None, poppler_path=None)
        max_pages = info["Pages"]
        # create an empty numpy array to store the image paths
        images = np.array([], dtype=object)
        for page in range(1, max_pages+1, 10):
            images_: List[str] = convert_from_path(self.pdfpath, first_page=page, last_page=min(page+10-1,max_pages), output_folder=output_folder, paths_only=True, **kwargs)
            images = np.append(images, images_)
            images = np.unique(images)
            # images = np.concatenate([images, images_[~np.isin(images_,images)])

        # order the images by page number.
        for i, image in enumerate(images):
            image = Path(image)
            prefix = image.stem.split('-')[0]
            page_num = int(image.stem.split('-')[-1])
            new_name = f'{prefix}-{page_num}{image.suffix}'
            new_file = image.parent / new_name
            new_file.unlink(missing_ok=True)
            image.rename(new_file)
            images[i] = new_file

        #images = images.sort(key=lambda x: int(Path(x).stem.split('-')[-1]))
        # images_ = list(map(lambda x: int(Path(x).stem.split('-')[-1]), images))
        # images = images[np.argsort(images_)]
        images = sorted(images, key=lambda x: int(Path(x).stem.split('-')[-1]))

        return images


