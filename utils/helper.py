import os
import sys
import asyncio
from typing import Dict, List, Optional, SupportsFloat as Numeric
import itertools
from PIL import Image
from functools import partial
import signal
from concurrent.futures import ThreadPoolExecutor
from decimal import Decimal
# Use ProcessPoolExecutor instead, as pdfCropMargins has a global "args". but will cause memory leaks
# from concurrent.futures import ProcessPoolExecutor

if __name__ == '__main__':
    sys.path.append(os.path.abspath(os.path.join(
        os.path.dirname(__file__), os.path.pardir)))
sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(__file__), os.path.pardir)))

from common.env import CPU_NUM
from applib.log import logger
from applib.interrupt import KeyboardInterruptBlocked


def sync_exec(coro, *args, **kwargs):
    try:
        with KeyboardInterruptBlocked():
            loop = asyncio.get_event_loop()
            return loop.run_until_complete(coro(*args, **kwargs))

    except:
        logger.exception(f'An error occured while doing coroutine job {coro}')

def keyboardInterruptHandler(signal, frame):
    # 如果用sys.exit()会报系统异常达不到直接结束程序的效果。
    # 因为sys.exit()触发的是系统异常，Exception只能捕获由代码触发的异常。
    # 即使如此也不推荐使用sys.exit来结束进程，
    # 因为Ctrl-C退出的时候需要按三次才能完全退出（和进程数无关）

    print("PoolExecutor: KeyboardInterrupt (ID: {}) has been caught. Cleaning up...".format(signal))
    # asyncio.get_event_loop().stop()
    os._exit(0)


def pool_init():
    signal.signal(signal.SIGINT, keyboardInterruptHandler)


def tile(filename: str, dir_in: str, dir_out: str, d: int, width=None, height=None, ignore_partial=True, keep_source=True) -> List[str]:
    name, ext = os.path.splitext(filename)
    filepath = os.path.join(dir_in, filename)
    img = Image.open(filepath)
    w, h = img.size
    tiles = []

    d_width = d if width is None else width
    d_height = d if height is None else height

    h_parts = range(0, h-h % d_height, d_height)
    w_parts = range(0, w-w % d_width, d_width)

    grid = list(itertools.product(h_parts, w_parts))

    # for i, j in grid:
    for ii, (i, j) in enumerate(grid):
        left, upper, right, lower = j, i, j+d_width, i+d_height
        if not ignore_partial and ii + 1 == len(grid):
            right = w
            lower = h
        box = (left, upper, right, lower)
        out = os.path.join(dir_out, f'{name}_{i}_{j}{ext}')
        tiles.append(out)
        img.crop(box).save(out, quality=95, subsampling=0)

    if not keep_source:
        os.remove(filepath)

    return tiles

async def wait_file_ready(filepath: str) -> None:
    historicalSize = -1
    while (historicalSize != os.path.getsize(filepath)):
        historicalSize = os.path.getsize(filepath)
        await asyncio.sleep(1)

def merge_dict(dict1: Optional[Dict], dict2: Optional[Dict]) -> Dict:
    """Merge two dictionaries into new one."""
    new_dict = {}
    if dict1:
        new_dict.update(dict1)
    if dict2:
        new_dict.update(dict2)
    return new_dict

def round_half_up(num: Numeric) -> int:
    """Round half up."""
    # return int(base * round(float(num) / base))
    return int(Decimal(num).quantize(Decimal('1.'), rounding='ROUND_HALF_UP'))

if __name__ == '__main__':
    print(tile('af2a67c3-124b-4b82-b79d-5d88ed56a235-230001-1.png',
               './', './', 426, height=598, ignore_partial=False))
